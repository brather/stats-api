<?php
return [
    'viewReport' => [
        'type' => 2,
        'description' => 'View statistics report',
    ],
    'trackData' => [
        'type' => 2,
        'description' => 'Track statistics data',
    ],
    'reader' => [
        'type' => 1,
        'children' => [
            'viewReport',
        ],
    ],
    'writer' => [
        'type' => 1,
        'children' => [
            'trackData',
        ],
    ],
];
