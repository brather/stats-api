<?php

$config = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=lcoalhost;dbname=neb_stats',
    'username' => 'neb',
    'password' => '',
    'charset' => 'utf8',
];
if (file_exists(__DIR__ . '/local/db.php')) {
    $config = array_replace_recursive(
        $config,
        require __DIR__ . '/local/db.php'
    );
}
return $config;