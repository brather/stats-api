<?php

$params = require(__DIR__ . '/params.php');

/**
 * @todo ������� ����������� �� �����
 */
$config = [
    'id' => 'basic',
    'name' => 'Statistics ',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'timeZone' => 'UTC',
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'defaultTimeZone' => 'UTC',
        ],
        'request' => [
            'cookieValidationKey' => 'Azclv7cr18PF_wTxWYioO9_MeD9yg5cD',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/api.log',
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule', 'controller' => ['report']
                ],
                [
                    'class' => 'yii\rest\UrlRule', 'controller' => ['track']
                ],
                [
                    'class' => 'yii\rest\UrlRule', 'controller' => ['export']
                ],
                [
                    'class' => 'yii\rest\UrlRule', 'controller' => ['period-data']
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (file_exists(__DIR__ . '/local/api.php')) {
    $config = array_replace_recursive(
        $config,
        require __DIR__ . '/local/api.php'
    );
}

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => array('*'),
    ];
}

return $config;
