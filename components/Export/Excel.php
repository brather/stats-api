<?php
namespace app\components\Export;

use app\models\Stats\Report\AllWchzReport;

/**
 * User: agolodkov
 * Date: 21.01.2016
 * Time: 15:13
 */
class Excel extends ReportExport
{
    /**
     * @param string $path
     *
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function save($path)
    {
        $report = $this->getReport();
        $data = [
            'firstColumn' => 'Дата',
            'total'       => true,
            'aWidth'      => false,
        ];
        /** @todo выпилить костыль */
        if ($report instanceof AllWchzReport) {
            $data = [
                'firstColumn' => 'ВЧЗ',
                'total'       => false,
                'aWidth'      => true,
            ];
        }
        $title = 'Report - ' . $this->getReport()->getRequest()->getName()
            . ' ' . date(
                $this->getReport()->getRequest()->getDayFormat(),
                $this->getReport()->getRequest()->getDateFrom()
            )
            . ' - ' . date(
                $this->getReport()->getRequest()->getDayFormat(),
                $this->getReport()->getRequest()->getDateTo()
            );
        $excel = new \PHPExcel();
        $excel->getProperties()
            ->setTitle($title)
            ->setSubject($title);

        $sheet = $excel->setActiveSheetIndex(0);
        /** @todo подключить языковые файлы */
        $sheet
            ->setCellValue('A1', $data['firstColumn'])
            ->setCellValue('B1', $this->getReport()->getRequest()->getName());

        $index = 2;
        $yAxis = $this->getReport()->getYAxis();
        $xAxis = $this->getReport()->getXAxis();

        $total = 0;
        foreach ($yAxis['labels'] as $key => $label) {
            /** @todo добавить возможность выгружать все колонки из $xAxis['series'] а не только первую */
            $sheet
                ->setCellValue('A' . $index, $label)
                ->setCellValue('B' . $index, $xAxis['series'][0]['data'][$key]);
            $index++;
            $total += (integer)$xAxis['series'][0]['data'][$key];
        }
        if (true === $data['total']) {
            $sheet
                ->setCellValue('A' . $index, 'Итого')
                ->setCellValue('B' . $index, $total);
        }

        /** Считаем текущую ширину А */
        $sheet->calculateColumnWidths();
        /** Добавляем в А титл и считем ширину */
        $sheet->insertNewRowBefore();
        $sheet->setCellValue('A1', $title);
        $sheet->calculateColumnWidths();
        /** Мержым колонки и выставляем ширину */
        foreach (['A', 'B'] as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(false);
        }
        $sheet->mergeCells('A1:B1');
        if(true === $data['aWidth']) {
            $sheet->getColumnDimension('A')->setWidth(60);
        }
        $sheet->getColumnDimension('B')->setWidth(strlen($title) * 0.7);

        $excel->getActiveSheet()->setTitle('Statistics');
        $excel->setActiveSheetIndex(0);

        /** @todo выпилить бяку */
        if ('php://output' === $path) {
            header('Content-Type: application/vnd.ms-excel');
            header(
                'Content-Disposition: attachment;filename="' . $title . '.xlsx"'
            );
            header(
                'Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'
            );
        }

        $excelWriter = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $excelWriter->save($path);
    }
}