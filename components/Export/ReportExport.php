<?php
namespace app\components\Export;

use app\models\Stats\Report;
use yii\base\Component;

/**
 * User: agolodkov
 * Date: 21.01.2016
 * Time: 15:12
 */
abstract class ReportExport extends Component
{
    /**
     * @var Report
     */
    public $report;

    /**
     * @param string $format
     * @param Report $report
     *
     * @return ReportExport
     * @throws ExportException
     */
    public static function factory($format, $report)
    {
        $component = 'app\components\Export\\' . ucfirst($format);
        if (!class_exists($component)) {
            throw new ExportException("Unknown format '$format'");
        }

        return new $component(['report' => $report]);
    }

    abstract public function save($path);

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param Report $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

}