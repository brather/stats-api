<?php
namespace app\commands;

use app\models\Action\Action;
use app\models\Stats\Aggregation\AggregatedDailyReportData;
use app\models\Stats\Report;
use app\models\Stats\ReportRequest;
use yii\console\Controller;

class AggregationController extends Controller
{

    public $batchSize = 30;
    public $startDate;

    private $_reportParams;

    public function options($actionID)
    {
        return ['batchSize', 'startDate'];
    }

    private function _loadReportParams()
    {
        $this->_reportParams = Report::getReportParams();
        foreach ($this->_reportParams as $reportCode => $itemParams) {
            if (!isset($itemParams['aggregate'])
                || empty($itemParams['aggregate'])
            ) {
                unset($this->_reportParams[$reportCode]);
            }
        }
    }

    public function actionAggregate()
    {
        $this->stdout("Starting aggregation process\n");
        $this->_loadReportParams();
        foreach ($this->_reportParams as $reportCode => $itemParams) {
            $batchQueue = $this->_buildDaysBatchQueue($reportCode);
            $reportDaysCount = 0;
            echo "Start aggregation report '$reportCode'", PHP_EOL;
            foreach ($batchQueue as $batch) {
                $class = 'app\models\Stats\Report\\'
                    . $itemParams['report'];
                $request = new ReportRequest();
                $requestParams = [
                    'dateFrom'  => $batch['from'],
                    'dateTo'    => $batch['to'],
                    'dayFormat' => 'Y-m-d',
                ];
                $request->load(
                    $requestParams, ''
                );
                $params = $itemParams;
                $params['request'] = $request;
                unset($params['report']);
                /** @var Report $report */
                $report = new $class($params);
                $report->load($params, '');
                $report->validate();
                $report->loadData();
                $yAxis = $report->getYAxis();
                $xAxis = $report->getXAxis();
                $dates = [];
                $aggregationModels = [];
                foreach ($yAxis['labels'] as $key => $label) {
                    $dates[$label] = $key;
                    $aggregationModels[$label] = null;
                }
                if (!empty($dates)) {
                    /** @var AggregatedDailyReportData[] $aggregations */
                    $aggregations = AggregatedDailyReportData::find()
                        ->where(
                            [
                                'report_code' => $reportCode,
                                'date'        => array_keys($dates),
                            ]
                        )
                        ->all();
                    foreach ($aggregations as $aggregation) {
                        $key = isset($dates[$aggregation->date])
                            ? $dates[$aggregation->date] : null;
                        if (isset($key)
                            && isset($xAxis['series'][0]['data'][$key])
                        ) {
                            $value
                                = $xAxis['series'][0]['data'][$key];
                            $aggregation->value = (integer)$value;
                            $aggregationModels[$aggregation->date]
                                = $aggregation;
                        }
                    }
                }
                foreach ($aggregationModels as $keyDate => $aggregation) {
                    if (!$aggregation instanceof AggregatedDailyReportData
                        && isset($xAxis['series'][0]['data'][$dates[$keyDate]])
                    ) {
                        $value = $xAxis['series'][0]['data'][$dates[$keyDate]];
                        $aggregation = new AggregatedDailyReportData();
                        $aggregation->load(
                            [
                                'report_code' => $reportCode,
                                'value'       => $value,
                                'date'        => $keyDate,
                            ], ''
                        );
                        $aggregationModels[$keyDate] = $aggregation;
                    }
                }
                foreach ($aggregationModels as $aggregation) {
                    if ($aggregation instanceof AggregatedDailyReportData) {
                        if ($aggregation->save()) {
                            $reportDaysCount++;
                            echo "Save days $reportDaysCount\r";
                        }
                    }
                }
            }
            echo PHP_EOL, PHP_EOL;
        }

        return 0;
    }

    private function _buildDaysBatchQueue($reportCode)
    {
        $startDate = $this->startDate;
        if (empty($startDate)) {
            $query = AggregatedDailyReportData::find();
            $query->where(['report_code' => $reportCode]);
            $startDate = $query->max('date');
        }
        if (empty($startDate)) {
            $actionName = $this->_reportParams[$reportCode]['action'];
            $query = Action::find();
            $query->where(['action' => $actionName]);
            $startDate = $query->min('DATE(timestamp)');
        }
        $batchQueue = [];
        if (!empty($startDate)) {
            $currentTime = time();
            $dateTime = strtotime($startDate);
            $daysCount = 0;
            $dateFrom = $dateTime;
            if (($currentTime - $dateTime) < 86400) {
                $batchQueue = [
                    [
                        'from' => $dateTime,
                        'to'   => $currentTime,
                    ]
                ];
            }
            while (($currentTime - $dateTime) >= 86400) {
                $dateTime += 86400;
                $daysCount++;
                if ($daysCount >= $this->batchSize) {
                    $batchQueue[] = [
                        'from' => $dateFrom,
                        'to'   => $dateTime,
                    ];
                    $dateFrom = $dateTime;
                    $dateFrom += 86400;
                    $daysCount = 0;
                }
            }
            if ($daysCount > 0) {
                $batchQueue[] = [
                    'from' => $dateFrom,
                    'to'   => $dateTime,
                ];
            }
        }

        return $batchQueue;
    }

}