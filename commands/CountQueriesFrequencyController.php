<?php
namespace app\commands;

use app\models\WchzList;
use yii\console\Controller;

/**
 * User: agolodkov
 * Date: 08.02.2016
 * Time: 15:54
 */
class CountQueriesFrequencyController extends Controller
{

    /**
     * @return int
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionCount()
    {
        $connection = \Yii::$app->getDb();
        $transaction = $connection->beginTransaction();
        try {
            $connection->createCommand(
                "DELETE FROM neb_data_values WHERE code = 'neb_search_queries'"
            )->execute();
            $result = $connection->createCommand(
                "
INSERT INTO neb_data_values (data, code, value)
  SELECT
    query        data,
    'neb_search_queries',
    count(query) value
  FROM neb_search_action
  GROUP BY query;"
            )->query();
            $count = $result->getRowCount();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        $this->stdout("Inserted $count queries \n");

        return $count;
    }
}