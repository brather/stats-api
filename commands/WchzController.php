<?php
namespace app\commands;

use app\models\WchzList;
use yii\console\Controller;

/**
 * User: agolodkov
 * Date: 08.02.2016
 * Time: 15:54
 */
class WchzController extends Controller
{
    public function actionLoad()
    {
        $this->stdout("Starting load wchz list\n");

        /** \yii\db\Connection */
        $connection = \Yii::$app->db;

        /** @var \GuzzleHttp\Client $nebClient */
        $nebClient = \Yii::$app->nebClient;
        $request = $nebClient->get('rest_api/wchz/?active=a');
        $arSiteData = json_decode($request->getBody(), true);
        if (empty($arSiteData['ITEMS'])) {
            $this->stdout("Wchz list is empty\n");
            return 1;
        }
        //print_r($arSiteData); die();
        //$connection->createCommand()->truncateTable(WchzList::tableName())->execute();

        $arResult = ['ADD' => 0, 'UPDATE' => 0, 'TOTAL' => 0];

        // получение всех ВЧЗ
        $arDbWchz = $arSiteWchz = [];
        $dbWchz = WchzList::find()->all();
        foreach ($dbWchz as $wchz) {
            $arDbWchz[$wchz->wchz_id][$wchz->ip] = [
                'name'           => $wchz->name,
                'wchz_id'        => $wchz->wchz_id,
                'active'         => $wchz->active,
                'ip'             => $wchz->ip,
                'ip_to'          => $wchz->ip_to,
                'neb_library_id' => $wchz->neb_library_id,
            ];
        }

        // добавление / обновление ВЧЗ
        foreach ($arSiteData['ITEMS'] as $wchzItem) {
            $ips = $wchzItem['PROPERTY_IP_VALUE'];
            foreach ($ips as &$ip) {
                if (false === strpos($ip, '-')) {
                    $ip = ip2long($ip);
                } else {
                    $ip = explode('-', $ip);
                    $ip[0] = ip2long($ip[0]);
                    $ip[1] = ip2long($ip[1]);
                }
            }
            unset($ip);
            foreach ($ips as $ip) {

                $params = [
                    'wchz_id'        => $wchzItem['ID'],
                    'name'           => $wchzItem['NAME'],
                    'active'         => $wchzItem['ACTIVE'],
                    'neb_library_id' => intval($wchzItem['PROPERTY_LIBRARY_VALUE']),
                ];
                if (is_array($ip)) {
                    $params['ip'] = $iFindIp = $ip[0];
                    $params['ip_to'] = $ip[1];
                } else {
                    $params['ip'] = $iFindIp = $ip;
                }

                $arSiteWchz[$params['wchz_id']][$params['ip']] = $params; // понадобится ниже

                // добавление
                $arDbItem = $arDbWchz[$wchzItem['ID']][$iFindIp];
                if (empty($arDbItem) && $iFindIp > 0) {

                    $connection->createCommand()->insert(WchzList::tableName(), $params)->execute();
                    $arResult['ADD']++;
                }
                // обновление
                else {

                    $sDbStr = $arDbItem['name'] . $arDbItem['wchz_id'] . $arDbItem['active']
                         . $arDbItem['ip'] . intval($arDbItem['ip_to']) . $arDbItem['neb_library_id'];

                    $sSiteStr = $params['name'] . $params['wchz_id'] . $params['active']
                        . $params['ip'] . intval($params['ip_to']) . $params['neb_library_id'];

                    if ($sDbStr != $sSiteStr) {
                        //print_r([$sDbStr, $sSiteStr]); echo "\n\r";

                        $model = WchzList::findOne([
                            'wchz_id' => $params['wchz_id'],
                            'ip'      => $params['ip'],
                        ]);

                        if ($model->wchz_id > 0) {
                            $connection->createCommand()->update(
                                WchzList::tableName(),
                                [
                                    'name'           => $params['name'],
                                    'active'         => $params['active'],
                                    'ip'             => $params['ip'],
                                    'ip_to'          => $params['ip_to'],
                                    'neb_library_id' => $params['neb_library_id'],
                                ],
                                [
                                    'wchz_id' => $params['wchz_id'],
                                    'ip'      => $params['ip']
                                ]
                            )->execute();
                            $arResult['UPDATE']++;
                        }
                    }
                }

                $arResult['TOTAL']++;
            }
        }

        // деактивация удаленных из Битрикса ВЧЗ
        foreach ($arDbWchz as $iWchz => $arAddress) {
            foreach ($arAddress as $iIp => $arWchz) {
                if (empty($arSiteWchz[$iWchz][$iIp]) && $arWchz['active'] != "D") {
                    $connection->createCommand()->update(
                        WchzList::tableName(),
                        ['active' => 'D'],
                        ['wchz_id' => $iWchz, 'ip' => $iIp]
                    )->execute();
                    $arResult['UPDATE']++;
                }
            }
        }

        $this->stdout("Add ".$arResult['ADD']." rows \n");
        $this->stdout("Update ".$arResult['UPDATE']." rows \n");
        $this->stdout("Total ".$arResult['TOTAL']." rows \n");

        return 0;
    }
}