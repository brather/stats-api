<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $viewReport = $auth->createPermission('viewReport');
        $viewReport->description = 'View statistics report';
        $auth->add($viewReport);

        $trackData = $auth->createPermission('trackData');
        $trackData->description = 'Track statistics data';
        $auth->add($trackData);


        $reader = $auth->createRole('reader');
        $auth->add($reader);
        $auth->addChild($reader, $viewReport);

        $writer = $auth->createRole('writer');
        $auth->add($writer);
        $auth->addChild($writer, $trackData);

        $auth->assign($writer, 1);
        $auth->assign($reader, 2);
    }
}