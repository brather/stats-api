<?php

use yii\db\Migration;

class m161111_122027_fill_action_wchz_ids extends Migration
{
    public function up()
    {
        $ready = 0;

        $connection = $this->getDb();

        $list = \app\models\WchzList::find()->all();

        echo PHP_EOL;

        /** @var \app\models\WchzList $ob */
        foreach ($list as $ob) {

            $wchz_id = $ob->wchz_id;
            $ip      = $ob->ip;
            $ip_to   = $ob->ip_to;

            if ($ip_to == 0) {
                $sWhere = "ip = " . $ip;
            } else {
                $sWhere = "ip >= " . $ip . " AND ip <= " . $ip_to;
            }

            $sql = "UPDATE neb_action SET wchz_id = $wchz_id WHERE $sWhere;";
            $connection->createCommand($sql)->execute();

            $ready++;
            echo "Saved items: $ready\r";
        }
    }

    public function down()
    {
        return false;
    }
}