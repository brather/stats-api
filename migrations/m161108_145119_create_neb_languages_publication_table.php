<?php

use yii\db\Migration;

/**
 * Handles the creation of table `neb_languages_publication`.
 */
class m161108_145119_create_neb_languages_publication_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(
            'neb_languages_publication',
            [
                'id' => $this->primaryKey(),
                'tr_type' => $this->string(255)->notNull(),
                'cr_type' => $this->string(255)->notNull(),
                'data_id' => $this->integer()->notNull(),
                'library_id' => $this->integer()->notNull(),
                'neb_library_id' => $this->integer(),
            ],
            'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
        );

        $this->createIndex(
            'idx-lng-pub',
            'neb_languages_publication',
            array('data_id', 'tr_type'),
            true
        );
        $this->createIndex(
            'idx-lng-pub-2',
            'neb_languages_publication',
            array('data_id', 'cr_type'),
            true
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('neb_languages_publication');

        $this->dropIndex('idx-lng-pub', 'neb_languages_publication');

        $this->dropIndex('idx-lng-pub-2', 'neb_languages_publication');
    }
}
