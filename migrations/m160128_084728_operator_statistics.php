<?php

use yii\db\Migration;

class m160128_084728_operator_statistics extends Migration
{
    public function up()
    {
        if (null === $this->db->schema->getTableSchema(
                'neb_user_deletion_action', true
            )
        ) {
            $this->createTable(
                'neb_user_deletion_action', [
                    'id'         => $this->primaryKey(),
                    'action_id'  => $this->bigInteger()->notNull(),
                    'library_id' => $this->integer()->defaultValue(null),
                ]
            );
            $this->createIndex(
                'idx-neb_user_deletion_action-action_id',
                'neb_user_deletion_action',
                'action_id'
            );
            $this->createIndex(
                'idx-neb_user_deletion_action-library_id',
                'neb_user_deletion_action',
                'library_id'
            );
        }

        if (null === $this->db->schema->getTableSchema(
                'neb_user_bind_action', true
            )
        ) {
            $this->createTable(
                'neb_user_bind_action', [
                    'id'         => $this->primaryKey(),
                    'action_id'  => $this->bigInteger()->notNull(),
                    'library_id' => $this->integer()->defaultValue(null),
                ]
            );
            $this->createIndex(
                'idx-neb_user_bind_action-action_id', 'neb_user_bind_action',
                'action_id'
            );
            $this->createIndex(
                'idx-neb_user_bind_action-library_id', 'neb_user_bind_action',
                'library_id'
            );
        }

        $this->addColumn('neb_search_action', 'library_id', $this->integer());
    }

    public function down()
    {
        $this->dropTable('neb_user_deletion_action');
        $this->dropTable('neb_user_bind_action');
        $this->dropColumn('neb_search_action', 'library_id');

        return false;
    }
}
