<?php

use yii\db\Schema;
use yii\db\Migration;

class m160120_131620_create_action_tables extends Migration
{
    public function up()
    {

        $this->createTable(
            'neb_action', [
                'id'        => 'BIGINT NOT NULL AUTO_INCREMENT',
                'timestamp' => $this->timestamp()
                    . ' DEFAULT CURRENT_TIMESTAMP',
                'action'    => $this->string(30)->notNull(),
                'ip'        => $this->bigInteger(),
                'user_id'   => $this->integer()->notNull(),
                'PRIMARY KEY(id)'
            ]
        );
        $this->createIndex(
            'idx-neb_action-action', 'neb_action', 'action'
        );
        $this->createIndex(
            'idx-neb_action-timestamp', 'neb_action', 'timestamp'
        );
        $this->createIndex(
            'idx-neb_action-ip', 'neb_action', 'ip'
        );

        $this->createTable(
            'neb_book_action', [
                'id'         => $this->primaryKey(),
                'action_id'  => $this->bigInteger()->notNull(),
                'book_id'    => $this->string(100)->notNull(),
                'page_num'   => $this->integer()->notNull(),
                'session_id' => $this->string(100),
                'closed'     => $this->boolean()->defaultValue(false),
            ]
        );
        $this->createIndex(
            'idx-neb_book_action-action_id', 'neb_book_action', 'action_id'
        );

        $this->createTable(
            'neb_search_action', [
                'id'         => $this->primaryKey(),
                'action_id'  => $this->bigInteger()->notNull(),
                'query'      => $this->text(),
                'session_id' => $this->string(100),
            ]
        );
        $this->createIndex(
            'idx-neb_search_action-action_id', 'neb_search_action', 'action_id'
        );
    }

    public function down()
    {
        $this->dropTable('neb_action');
        $this->dropTable('neb_book_action');
        $this->dropTable('neb_search_action');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
