<?php

use yii\db\Migration;

class m160127_102735_library_stats extends Migration
{
    public function up()
    {
        if (null === $this->db->schema->getTableSchema('neb_user_auth', true)) {
            $this->createTable(
                'neb_user_auth', [
                    'id'         => $this->primaryKey(),
                    'action_id'  => $this->bigInteger()->notNull(),
                    'library_id' => $this->integer()->defaultValue(null),
                ]
            );
            $this->createIndex(
                'idx-neb_user_auth-action_id', 'neb_user_auth', 'action_id'
            );
            $this->createIndex(
                'idx-neb_user_auth-library_id', 'neb_user_auth', 'library_id'
            );
        }


        if (null === $this->db->schema->getTableSchema(
                'neb_daily_data', true
            )
        ) {
            $this->createTable(
                'neb_daily_data', [
                    'id'        => 'BIGINT NOT NULL AUTO_INCREMENT',
                    'date'      => $this->date()->notNull(),
                    'type'      => $this->string(100)->notNull(),
                    'value_num' => $this->bigInteger()->defaultValue(null),
                    'PRIMARY KEY(id)'
                ]
            );
            $this->createIndex(
                'idx-neb_daily_data-date', 'neb_daily_data', 'date'
            );
            $this->createIndex(
                'idx-neb_daily_data-type', 'neb_daily_data', 'type'
            );
        }


        if (null === $this->db->schema->getTableSchema(
                'neb_data_library', true
            )
        ) {
            $this->createTable(
                'neb_data_library', [
                    'id'          => $this->primaryKey(),
                    'period_type' => $this->string(30)->notNull(),
                    'data_id'     => $this->bigInteger()->notNull(),
                    'library_id'  => $this->integer()->notNull(),
                ]
            );

            $this->createIndex(
                'idx-neb_data_library-period_data_id',
                'neb_data_library',
                ['period_type', 'data_id']
            );
            $this->createIndex(
                'idx-neb_data_library-library_id',
                'neb_data_library',
                'library_id'
            );
        }
    }

    public function down()
    {
        $this->dropTable('neb_user_auth');
        $this->dropTable('neb_daily_data');
        $this->dropTable('neb_data_library');

        return false;
    }
}
