<?php

use yii\db\Migration;

/**
 * Handles the creation of table `neb_bbkfull`.
 */
class m161116_121958_create_neb_bbkfull_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if (null === $this->db->schema->getTableSchema('neb_bbkfull', true)) {
            
            $this->createTable(
                'neb_bbkfull',
                [
                    'id' => $this->primaryKey(),
                    'title' => $this->text()->notNull(),
                    'path' => $this->text()->notNull(),
                    'pathId' => $this->text()->notNull(),
                    'fullPath' => $this->text()->notNull(),
                    'depth' => $this->integer(5)->notNull(),
                    'data_id' => $this->bigInteger()->notNull(),
                    'library_id' => $this->integer(11)->notNull()->defaultValue(0),
                    'neb_library_id' => $this->integer(11)->defaultValue(null),
                ],
                'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
            );

            $this->createIndex(
                "idx_neb_bbkfull_data_id-library_id",
                "neb_bbkfull",
                array("data_id", "library_id"),
                true
            );

            $this->createIndex(
                "idx_neb_bbkfull_data_id-depth" .
                "neb_bbkfull",
                array("data_id", "depth"),
                true
            );

            $this->createIndex(
                "idx_neb_bbkfull_data_id-library_id-neb_library_id",
                "neb_bbkfull",
                array("data_id", "library_id", "neb_library_id"),
                true
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('neb_bbkfull');

        $this->dropIndex("idx_neb_bbkfull_data_id-depth", "neb_bbkfull");
        $this->dropIndex("idx_neb_bbkfull_data_id-library_id", "neb_bbkfull");
        $this->dropIndex("idx_neb_bbkfull_data_id-library_id-neb_library_id", "neb_bbkfull");
    }
}
