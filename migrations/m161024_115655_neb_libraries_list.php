<?php

use yii\db\Schema;
use yii\db\Migration;

class m161024_115655_neb_libraries_list extends Migration
{
    public function up()
    {
        $this->createTable(
            'neb_libraries_list', [
                'id'             => $this->primaryKey(),
                'name'           => $this->string(512)->notNull(),
                'neb_partner_id' => $this->integer(),
            ],
            'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB'
        );

        $this->createIndex(
            'idx-neb_libraries_list-neb_partner_id', 'neb_libraries_list', 'neb_partner_id'
        );

        $this->addColumn('neb_action', 'wchz_id', $this->integer());
        $this->createIndex('idx-neb_action-wchz_id', 'neb_action', 'wchz_id');

        $this->addColumn('neb_book_action', 'neb_library_id', $this->integer());
        $this->createIndex('idx-neb_book_action-neb_library_id', 'neb_book_action', 'neb_library_id');

        $this->addColumn('neb_book_change_access_action', 'neb_library_id', $this->integer());
        $this->createIndex(
            'idx-neb_book_change_access_action-neb_library_id', 'neb_book_change_access_action', 'neb_library_id'
        );

        $this->addColumn('neb_data_library', 'neb_library_id', $this->integer());
        $this->createIndex('idx-neb_data_library-neb_library_id', 'neb_data_library', 'neb_library_id');

        $this->addColumn('neb_search_action', 'neb_library_id', $this->integer());
        $this->createIndex('idx-neb_search_action-neb_library_id', 'neb_search_action', 'neb_library_id');

        $this->addColumn('neb_user_auth', 'neb_library_id', $this->integer());
        $this->createIndex('idx-neb_user_auth-neb_library_id', 'neb_user_auth', 'neb_library_id');

        $this->addColumn('neb_user_bind_action', 'neb_library_id', $this->integer());
        $this->createIndex('idx-neb_user_bind_action-neb_library_id', 'neb_user_bind_action', 'neb_library_id');

        $this->addColumn('neb_user_deletion_action', 'neb_library_id', $this->integer());
        $this->createIndex('idx-neb_user_deletion_action-neb_library_id', 'neb_user_deletion_action', 'neb_library_id');

        $this->addColumn('neb_wchz_list', 'neb_library_id', $this->integer());
        $this->createIndex('idx-neb_wchz_list-neb_library_id', 'neb_wchz_list', 'neb_library_id');

    }

    public function down()
    {
        echo "m161024_115655_neb_libraries_list cannot be reverted.\n";

        return false;
    }
}
