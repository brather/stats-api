<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_121530_wchz_list extends Migration
{
    public function up()
    {
        if (null === $this->db->schema->getTableSchema(
                'neb_wchz_list', true
            )
        ) {
            $this->createTable(
                'neb_wchz_list', [
                    'name'    => $this->string(255)->notNull(),
                    'wchz_id' => $this->integer()->notNull(),
                    'ip'      => $this->bigInteger()->notNull(),
                    'ip_to'   => $this->bigInteger()->defaultValue(null),
                ]
            );
            $this->createIndex(
                'idx-neb_wchz_list-wchz_id', 'neb_wchz_list', 'wchz_id'
            );
            $this->createIndex(
                'idx-neb_wchz_list-ip', 'neb_wchz_list', 'ip'
            );
            $this->createIndex(
                'idx-neb_wchz_list-ip_to', 'neb_wchz_list', 'ip_to'
            );
        }
    }

    public function down()
    {
        $this->dropTable('neb_wchz_list');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
