<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_080025_books_library extends Migration
{
    public function up()
    {
        $this->addColumn('neb_book_action', 'library_id', $this->integer());

    }

    public function down()
    {
        $this->dropColumn('neb_book_action', 'library_id');

        return false;
    }
}
