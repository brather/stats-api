<?php

use yii\db\Migration;

/**
 * Handles the creation of table `neb_collection`.
 */
class m161109_064834_create_neb_collection_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(
            'neb_collection',
            [
                'id' => $this->primaryKey(),
                'title' => $this->string()->notNull(),
                'code' => $this->string()->notNull(),
                'data_id' => $this->integer()->notNull(),
                'ext_id' => $this->integer(),
                'library_id' => $this->integer()->notNull(),
                'neb_library_id' => $this->integer(),
            ],
            'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
        );

        $this->createIndex(
            "idx-neb_collection-data_id-title",
            "neb_collection",
            array(
                "data_id",
                "title"
            ),
            true
        );

        $this->createIndex(
            "idx-neb_collection-data_id-library_id-title",
            "neb_collection",
            array(
                "library_id",
                "data_id",
                "title"
            ),
            true
        );

        $this->createIndex(
            "idx-neb_collection-library_id-title",
            "neb_collection",
            array(
                "library_id",
                "title"
            ),
            false
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('neb_collection');

        $this->dropIndex("idx-neb_collection-data_id-title", "neb_collection");
        $this->dropIndex("idx-neb_collection-data_id-ext_id-title", "neb_collection");
        $this->dropIndex("idx-neb_collection-ext_id-title", "neb_collection");
    }
}
