<?php

use yii\db\Schema;
use yii\db\Migration;

class m161013_104038_neb_data_values extends Migration
{
    public function up()
    {
        $this->createTable(
            'neb_data_values', [
                'id'    => $this->primaryKey(),
                'code'  => $this->string(100)->notNull(),
                'data'  => $this->text(),
                'value' => $this->bigInteger()->notNull(),
            ]
        );
    }

    public function down()
    {
        echo "m161013_104038_neb_data_values cannot be reverted.\n";

        return false;
    }

}
