<?php

use yii\db\Schema;
use yii\db\Migration;

class m160725_120541_users_table extends Migration
{
    public function up()
    {
        $this->createTable(
            'neb_user', [
                'id'           => $this->primaryKey(),
                'login'        => $this->string(200)->notNull(),
                'password'     => $this->string(100)->notNull(),
                'group'        => $this->string(50),
                'access_token' => $this->string(200)->notNull(),
            ]
        );
        $security = Yii::$app->getSecurity();
        $user = new \app\models\User();
        $user->load(
            [
                'login'        => 'writer',
                'password'     => $security->generatePasswordHash($security->generateRandomString()),
                'group'        => 'writer',
                'access_token' => $security->generateRandomString(),
            ],
            ''
        );
        $user->save();

        $user = new \app\models\User();
        $user->load(
            [
                'login'        => 'reader',
                'password'     => $security->generatePasswordHash($security->generateRandomString()),
                'group'        => 'reader',
                'access_token' => $security->generateRandomString(),
            ],
            ''
        );
        $user->save();
    }

    public function down()
    {
        echo "m160725_120541_users_table cannot be reverted.\n";

        return false;
    }
}
