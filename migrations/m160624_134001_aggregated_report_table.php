<?php

use yii\db\Migration;

class m160624_134001_aggregated_report_table extends Migration
{
    public function up()
    {
        if (null === $this->db->schema->getTableSchema(
                'neb_aggregated_daily_report_data', true
            )
        ) {
            $this->createTable(
                'neb_aggregated_daily_report_data', [
                    'id'          => $this->primaryKey(),
                    'report_code' => $this->string(100)->notNull(),
                    'date'        => $this->date()->notNull(),
                    'value'       => $this->bigInteger()->notNull(),
                ]
            );
            $this->createIndex(
                'idx-neb_aggregated_daily_report_data-report_code',
                'neb_aggregated_daily_report_data',
                ['report_code', 'date'],
                true
            );
        }
    }

    public function down()
    {
        $this->dropTable('neb_aggregated_daily_report_data');

        return false;
    }
}
