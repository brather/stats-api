<?php

use yii\db\Migration;

/**
 * Handles the creation of table `neb_file_extension`.
 */
class m161115_084729_create_neb_file_extension_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if (null === $this->db->schema->getTableSchema('neb_file_extension', true)) {
            
            $this->createTable(
                'neb_file_extension',
                [
                    'id' => $this->primaryKey(),
                    'extension' => $this->string(8)->notNull(),
                    'data_id' => $this->bigInteger(20)->notNull(),
                    'library_id' => $this->integer()->notNull()->defaultValue(0),
                    'neb_library_id' => $this->integer()->defaultValue(null),
                ],
                'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
            );

            $this->createIndex(
                "idx-neb_file_extension-data_id-extension",
                "neb_file_extension",
                array(
                    "data_id",
                    "extension"
                ),
                true
            );

            $this->createIndex(
                "idx-neb_file_extension-data_id-extension-library_id",
                "neb_file_extension",
                array(
                    "data_id",
                    "extension",
                    "library_id"
                ),
                true
            );

            $this->createIndex(
                "idx-neb_file_ext-data_id-ext-lib_id-neb_lib_id",
                "neb_file_extension",
                array(
                    "data_id",
                    "extension",
                    "library_id",
                    "neb_library_id"
                ),
                true
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('neb_file_extension');

        $this->dropIndex("idx-neb_file_extension-data_id-extension", "neb_file_extension");
        $this->dropIndex("idx-neb_file_extension-data_id-extension-library_id", "neb_file_extension");
        $this->dropIndex("idx-neb_file_ext-data_id-ext-lib_id-neb_lib_id", "neb_file_extension");
    }
}
