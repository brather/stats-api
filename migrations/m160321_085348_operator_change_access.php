<?php

use yii\db\Schema;
use yii\db\Migration;

class m160321_085348_operator_change_access extends Migration
{
    public function up()
    {
        if (null === $this->db->schema->getTableSchema(
                'neb_book_change_access_action', true
            )
        ) {
            $this->createTable(
                'neb_book_change_access_action', [
                    'id'         => $this->primaryKey(),
                    'action_id'  => $this->bigInteger()->notNull(),
                    'library_id' => $this->integer()->notNull(),
                    'book_id'    => $this->string(100)->notNull(),
                    'comment'    => $this->text(),
                    'error'      => $this->text(),
                    'closed'     => $this->boolean(),
                ]
            );
            $this->createIndex(
                'idx-neb_book_change_access_action-action_id',
                'neb_book_action',
                'action_id'
            );
        }
    }

    public function down()
    {
        $this->dropTable('neb_book_change_access_action');
    }
}
