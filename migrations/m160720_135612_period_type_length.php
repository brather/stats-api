<?php

use yii\db\Migration;

class m160720_135612_period_type_length extends Migration
{
    public function up()
    {
        $this->alterColumn('neb_data_library', 'period_type', 'VARCHAR(100)');
    }

    public function down()
    {
        echo "m160720_135612_period_type_length cannot be reverted.\n";

        return false;
    }
}
