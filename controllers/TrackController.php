<?php
/**
 * User: agolodkov
 * Date: 20.01.2016
 * Time: 10:45
 */

namespace app\controllers;


use app\models\Action\Action;
use app\models\Action\TrackException;
use app\models\Action\TrackRequest;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController,
    app\models\LibrariesList;

class TrackController extends ActiveController
{
    public $modelClass = 'app\models\Action\TrackRequest';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow'   => true,
                    'actions' => ['create'],
                    'roles'   => ['trackData'],
                ],
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);

        return $actions;
    }

    /**
     * @return \app\models\Action\ActionActiveRecord
     * @throws TrackException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $unknownReason = 'Failed to create the object for unknown reason.';

        /** @var $request TrackRequest */
        $newRequest = $this->modelClass;
        $request = new $newRequest();

        $request->load(\Yii::$app->getRequest()->getBodyParams(), '');

        $data = $request->getData();
        $data['action'] = $request->getAction();

        $action = new Action();
        $action->load($data, '');
        if (!$action->save() && !$action->hasErrors()) {
            throw new TrackException($unknownReason);
        }

        $request->setDataItem('action_id', $action->getPrimaryKey());
        if ($request->modelExists()) {
            $actionModel = $request->forgeActionModel();
            /** @todo проверить ситуакцию с ошибками и отсутствием Эксепшена */
            if (!$actionModel->save() && !$actionModel->hasErrors()) {
                throw new TrackException($unknownReason);
            }
        }

        $response = \Yii::$app->getResponse();
        $response->setStatusCode(201);

        return $action;
    }
}