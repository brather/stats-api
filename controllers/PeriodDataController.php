<?php
/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 15:59
 */

namespace app\controllers;


use app\models\PeriodData\PeriodData;
use app\models\PeriodData\PeriodException;
use app\models\PeriodData\PeriodRequest;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

class PeriodDataController extends ActiveController
{
    public $modelClass = 'app\models\PeriodData\PeriodRequest';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow'   => true,
                    'actions' => ['create'],
                    'roles'   => ['trackData'],
                ],
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);

        return $actions;
    }

    /**
     * @return mixed
     * @throws PeriodException
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $unknownReason = 'Failed to create the object for unknown reason.';

        /** @var $request PeriodRequest */
        $request = new $this->modelClass();
        $bodyParams = \Yii::$app->getRequest()->getBodyParams();
        $request->load($bodyParams, '');

        $periodModel = PeriodData::factoryTableDataModel($request->getPeriod());
        $data = $request->getData();
        $data['type'] = $request->getType();
        $periodModel->load($data, '');
        $connection = PeriodData::getDb();
        $transaction = $connection->beginTransaction();
        /** @todo разобраться почему дата не добавляется через load() */
        $periodModel->setAttribute('date', $data['date']);
        if (!$periodModel->save() && !$periodModel->hasErrors()) {
            throw new PeriodException($unknownReason);
        }

        try {
            if ($request->modelExists()) {
                $request->setDataItem('data_id', $periodModel->getPrimaryKey());
                $request->setDataItem('period_type', $request->getType());
                $dataModel = $request->forgeDataModel();
                /** @todo проверить ситуакцию с ошибками и отсутствием Эксепшена */
                if (!$dataModel->save() && !$dataModel->hasErrors()) {
                    throw new PeriodException($unknownReason);
                }
                if ($dataModel->hasErrors()) {
                    $transaction->rollBack();
                    throw new PeriodException(implode(PHP_EOL, $dataModel->getFirstErrors()));
                }
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            return $e->getMessage();
        }

        $response = \Yii::$app->getResponse();
        $response->setStatusCode(201);

        return array(
            'periodMode'    => $periodModel,
            'dataModel'     => (isset($dataModel))?$dataModel : "something wrong",
            'bodyParams'    => $bodyParams
        );
    }
}