<?php
/**
 * User: agolodkov
 * Date: 19.01.2016
 * Time: 16:07
 */

namespace app\controllers;


use app\models\Stats\Report;
use app\models\Stats\ReportRequest;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

class FreeReportController extends ActiveController
{
    public $modelClass = 'app\models\ReportRequest';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow'   => true,
                    'actions' => ['index'],
                    'roles'   => ['viewReport'],
                ],
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);

        return $actions;
    }

    /**
     * @return array
     */
    public function actionIndex()
    {
        $request = new ReportRequest();
        $request->load(\Yii::$app->request->get(), '');
        $request->validate();
        $report = Report::factory($request);
        $report->loadData();

        return $report;
    }
}