<?php

// comment out the following two lines when deployed to production
$env = 'prod';
if (file_exists(__DIR__ . '/../env.php')) {
    $env = require __DIR__ . '/../env.php';
}
defined('YII_DEBUG') or define('YII_DEBUG', 'dev' === $env);
defined('YII_ENV') or define('YII_ENV', $env);
header('Access-Control-Allow-Origin: *');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/api.php');

(new yii\web\Application($config))->run();
