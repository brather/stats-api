<?php
/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 14:48
 */

namespace app\helpers;


class TableModel
{
    /**
     * @param string $table
     * @param string $namespace
     *
     * @return bool|string
     */
    public static function getTableModelName($table, $namespace)
    {
        $model = $namespace
            . str_replace(
                ' ',
                '',
                ucwords(
                    str_replace(
                        '_',
                        ' ',
                        $table
                    )
                )
            );
        if (class_exists($model)) {
            return $model;
        }

        return false;
    }
}