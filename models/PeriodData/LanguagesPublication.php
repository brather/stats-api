<?php

namespace app\models\PeriodData;

/**
 * This is the model class for table "neb_languages_publication".
 *
 * @property integer $id
 * @property string $tr_type
 * @property string $cr_type
 * @property integer $data_id
 */
class LanguagesPublication extends PeriodDataSubTable
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_languages_publication';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tr_type', 'cr_type', 'data_id', 'library_id'], 'required'],
            [['data_id', 'library_id', 'neb_library_id'], 'integer'],
            [['tr_type', 'cr_type'], 'string', 'max' => 255],
            [['data_id', 'tr_type'], 'unique', 'targetAttribute' => ['data_id', 'tr_type'], 'message' => 'The combination of Tr Type and Data ID has already been taken.'],
            [['data_id', 'cr_type'], 'unique', 'targetAttribute' => ['data_id', 'cr_type'], 'message' => 'The combination of Cr Type and Data ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tr_type' => 'Transcript Type',
            'cr_type' => 'Cyrillic Type',
            'data_id' => 'Data ID',
            'library_id' => 'Library ID',
            'neb_library_id' => 'NEB Partner library ID',
        ];
    }
}
