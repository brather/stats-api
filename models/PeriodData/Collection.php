<?php

namespace app\models\PeriodData;

/**
 * This is the model class for table "neb_collection".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property integer $data_id
 * @property integer $ext_id
 */
class Collection extends PeriodDataSubTable
{
    /*public $title;
    public $code;
    public $data_id;
    public $ext_id;
    public $library_id;
    public $neb_library_id;*/

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code', 'data_id', 'library_id'], 'required'],
            [['data_id', 'ext_id', 'library_id', 'neb_library_id'], 'integer'],
            [['title', 'code'], 'string', 'max' => 255],
            [['data_id', 'title'], 'unique', 'targetAttribute' => ['data_id', 'title'], 'message' => 'The combination of Title and Data ID has already been taken.'],
            [['library_id', 'data_id', 'title'], 'unique', 'targetAttribute' => ['library_id', 'data_id', 'title'], 'message' => 'The combination of Title, Data ID and Ext ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'code' => 'Code',
            'data_id' => 'Data ID',
            'ext_id' => 'External ID',
            'library_id' => 'Library ID',
            'neb_library_id' => 'NEB Partner library ID',
        ];
    }

}
