<?php
namespace app\models\PeriodData;

use app\helpers\TableModel;
use yii\base\Exception;
use yii\db\ActiveRecord,
    app\models\LibrariesList;

/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 14:35
 */
class PeriodData extends ActiveRecord
{

    /**
     * @todo ��������� � rules
     * @var int
     */
    public $max;

    /**
     * @todo ��������� � rules
     * @var string
     */
    public $date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'type'], 'required'],
            [['date'], 'date', 'format' => 'yyyy-M-d'],
            [['type'], 'string', 'max' => 100],
            [['value_num'], 'integer'],
        ];
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public static function getDataTable($action)
    {
        $actionTables = [
            'library_books'                             => 'data_library',
            'library_books_digitized'                   => 'data_library',
            'library_user_registers'                    => 'data_library',
            'library_service_cnt_book_downloads'        => 'data_library',
            'library_service_cnt_book_views'            => 'data_library',
            'library_service_cnt_book_downloads_unique' => 'data_library',
            'library_service_cnt_book_views_unique'     => 'data_library',
            'publication_is_protected'                  => 'data_library',
            'publication_not_protected'                 => 'data_library',
            'publication_copyright'                     => 'data_library',
            'publication_markup'                        => 'data_library',
            'language'                                  => 'languages_publication',
            'collection'                                => 'collection',
            'new_collection'                            => 'collection',
            'file_extension'                            => 'file_extension',
            'bbkfull'                                   => 'bbkfull',
        ];

        if (isset($actionTables[$action])) {
            return $actionTables[$action];
        }

        return '';
    }

    /**
     * @param string $type
     *
     * @return bool|string
     */
    public static function getDataModelName($type)
    {
        return TableModel::getTableModelName(
            static::getDataTable($type),
            'app\models\PeriodData\\'
        );
    }

    /**
     * @param string $type
     *
     * @return PeriodData
     * @throws \Exception
     */
    public static function factoryDataModel($type)
    {
        $model = static::getDataModelName($type);
        if (!class_exists($model)) {
            throw new \Exception("Data model for type '$type' not exists");
        }

        return new $model();
    }

    /**
     * @param string $table
     *
     * @return mixed
     * @throws \Exception
     */
    public static function factoryTableDataModel($table)
    {
        $model = TableModel::getTableModelName(
            $table,
            'app\models\PeriodData\\'
        );
        if (!class_exists($model)) {
            throw new \Exception("Data model for table '$table' not exists");
        }

        return new $model();
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->_setNebLibraryId();

            return true;
        }

        return false;
    }

    private function _setNebLibraryId ()
    {
        if (
            isset($this->library_id) && !empty($this->library_id)
            && (
                !isset($this->neb_library_id) || empty($this->neb_library_id)
            )
        )
        {
            $librariesList = LibrariesList::find()
                ->select( "id" )
                ->where(
                    array( "=", "neb_partner_id", (int)$this->library_id )
                )->one();

            if (!$librariesList->id)
                $this->neb_library_id = null;
            else
                $this->neb_library_id = $librariesList->id;
        }

        return true;
    }

}