<?php
/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 15:44
 */

namespace app\models\PeriodData;

use yii\base\Model;

class PeriodRequest extends Model
{
    /**
     * @var string
     */
    public $type;

    /**
     * @var
     */
    public $period;

    /**
     * @var array
     */
    public $data;


    public function rules()
    {
        return [
            [['type', 'period'], 'string'],
            [['type', 'data', 'period'], 'required']
        ];
    }

    /**
     * @return PeriodData
     */
    public function forgeDataModel()
    {
        $model = PeriodData::factoryDataModel($this->getType());
        $model->load($this->getData(), '');

        return $model;
    }

    /**
     * @return bool
     */
    public function modelExists()
    {
        return false !== PeriodData::getDataModelName($this->getType());
    }

    /**
     * @param string $dataField
     * @param mixed  $value
     */
    public function setDataItem($dataField, $value)
    {
        if (!is_array($this->data)) {
            $this->data = [];
        }
        $this->data[$dataField] = $value;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param mixed $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }


}