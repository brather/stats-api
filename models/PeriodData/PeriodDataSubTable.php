<?php
/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 15:08
 */

namespace app\models\PeriodData;


use app\helpers\TableModel;

class PeriodDataSubTable extends PeriodData
{

    /**
     * @param string $table
     *
     * @return $this
     * @throws PeriodException
     */
    protected function _getPeriodData($table)
    {
        if (empty($table)) {
            throw new PeriodException('Period table not is set');
        }
        /** @var PeriodData $tableModel */
        $tableModel = TableModel::getTableModelName(
            $table,
            __NAMESPACE__ . '\\'
        );
        if (!class_exists($tableModel)) {
            throw new PeriodException(
                "Data model for period table '{$table}' not found"
            );
        }

        return $this->hasOne($tableModel::className(), ['id' => 'data_id'])
            ->from($tableModel::tableName() . ' period_data');
    }

    public function getDaily()
    {
        return $this->_getPeriodData('daily_data');
    }
}