<?php

namespace app\models\PeriodData;

/**
 * This is the model class for table "neb_bbkfull".
 *
 * @property integer $id
 * @property string $title
 * @property string $path
 * @property string $pathId
 * @property string $fullPath
 * @property integer $data_id
 * @property integer $library_id
 * @property integer $neb_library_id
 */
class Bbkfull extends PeriodDataSubTable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_bbkfull';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'path', 'pathId', 'fullPath', 'data_id', 'library_id', 'depth'], 'required'],
            [['title', 'path', 'pathId', 'fullPath'], 'string'],
            [['data_id', 'library_id', 'neb_library_id', 'depth'], 'integer'],
            [['data_id', 'library_id'], 'unique', 'targetAttribute' => ['data_id', 'library_id'], 'message' => 'The combination of Data ID and Library ID has already been taken.'],
            [['data_id', 'library_id', 'neb_library_id'], 'unique', 'targetAttribute' => ['data_id', 'library_id', 'neb_library_id'], 'message' => 'The combination of Data ID, Library ID and Neb Library ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'path' => 'Path',
            'pathId' => 'Path ID',
            'fullPath' => 'Full Path',
            'data_id' => 'Data ID',
            'depth' => 'Depth',
            'library_id' => 'Library ID',
            'neb_library_id' => 'Neb Library ID',
        ];
    }
}
