<?php
/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 15:06
 */

namespace app\models\PeriodData;


/**
 * Class DataLibrary
 *
 * @package app\models\PeriodData
 */
class DataLibrary extends PeriodDataSubTable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_data_library';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period_type', 'data_id'], 'required'],
            [['period_type'], 'string', 'max' => 100],
            [['data_id'], 'integer'],
            [['library_id'], 'integer'],
            [['library_id'], 'default', 'value' => 0],
        ];
    }
}