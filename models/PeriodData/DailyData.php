<?php
/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 14:39
 */

namespace app\models\PeriodData;


class DailyData extends PeriodData
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_daily_data';
    }
}