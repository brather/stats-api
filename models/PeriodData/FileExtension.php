<?php

namespace app\models\PeriodData;

/**
 * This is the model class for table "neb_file_extension".
 *
 * @property integer $id
 * @property string $extension
 * @property integer $data_id
 * @property integer $library_id
 * @property integer $neb_library_id
 */
class FileExtension extends PeriodDataSubTable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_file_extension';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['extension', 'data_id', 'library_id'], 'required'],
            [['data_id', 'library_id', 'neb_library_id'], 'integer'],
            [['extension',], 'string', 'max' => 8],
            [['data_id', 'extension'], 'unique', 'targetAttribute' => ['data_id', 'extension'], 'message' => 'The combination of Extension and Data ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'extension' => 'Extension',
            'data_id' => 'Data ID',
            'library_id' => 'Library ID',
            'neb_library_id' => 'NEB Partner library ID',
        ];
    }
}
