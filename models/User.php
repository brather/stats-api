<?php
/**
 * User: agolodkov
 * Date: 25.07.2016
 * Time: 15:04
 */

namespace app\models;


use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{

    public function rules()
    {
        return [
            [['login', 'password', 'access_token'], 'required'],
            [['login', 'password', 'access_token', 'group'], 'string'],
            [['access_token'], 'default', 'value' => function () {
                return \Yii::$app->getSecurity()->generateRandomString();
            }]
        ];
    }

    public static function tableName()
    {
        return 'neb_user';
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     *
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @param mixed  $type  the type of the token. The value of this parameter depends on the implementation.
     *
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return '';
    }

    /**
     * @param string $authKey
     *
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}