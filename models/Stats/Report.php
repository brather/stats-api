<?php
/**
 * User: agolodkov
 * Date: 19.01.2016
 * Time: 16:06
 */

namespace app\models\Stats;


use yii\base\Model;
use yii\db\ActiveQuery;

abstract class Report extends Model
{
    /**
     * @var ReportRequest
     */
    public $request;


    /**
     * [
     *      'labels' => ['one', 'dos', 'tress'],
     * ];
     *
     * @var array
     */
    public $yAxis = [];

    /**
     * [
     *      'series' => [
     *          ['name' => 'One', 'data' => [1, 2, 3]],
     *          ['name' => 'Plus3', 'data' => [4, 5, 6]]
     *      ]
     * ];
     *
     * @var array
     */
    public $xAxis = [];

    /**
     * @var string
     */
    public $action;

    /**
     * @var string
     */
    public $periodItem;

    /**
     * @var array
     */
    public $meta;

    /**
     * @var bool
     */
    public $aggregate;


    /**
     * @return ActiveQuery
     */
    abstract protected function _buildQuery();

    /**
     * @param ActiveQuery $query
     */
    abstract protected function _prepareQuery(ActiveQuery $query);

    /**
     * @return $this
     */
    public function loadData()
    {
        $this->_loadMetaData();
    }

    protected function _loadMetaData()
    {
        if (isset($this->meta['totalCount'])) {
            $query = $this->_buildQuery();
            $this->_prepareQuery($query);
            $query->groupBy([]);
            $result = $query->one();
            $this->meta['totalCount'] = [$result->cnt];
        }
    }

    /**
     * @param int $daysCount
     *
     * @return mixed|string
     */
    public static function getPeriodItemCode($daysCount)
    {
        $periodCodes = [
            40  => 'twoDays',
            80  => 'fourDays',
            140 => 'week',
            210 => 'twoWeeks',
            450 => 'month',
        ];
        $count = key($periodCodes);
        $code = 'day';
        if ($daysCount >= $count) {
            $code = current($periodCodes);
            while (next($periodCodes)) {
                if (
                    $daysCount <= key($periodCodes)
                    && $daysCount >= $count
                ) {
                    break;
                }
                $count = key($periodCodes);
                $code = current($periodCodes);
            }
        }

        return $code;
    }

    /**
     * @return array
     */
    public function reportRules()
    {
        return [];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(
            [
                [['request', 'action'], 'required'],
                [
                    [
                        'request',
                        'yAxis',
                        'xAxis',
                        'action',
                        'periodItem',
                        'meta',
                    ],
                    'safe'
                ],
                [
                    /**
                     * ���������� ��������� �������� ����� ������� � ������
                     *
                     * @see Report::getPeriodItemCode();
                     */
                    ['periodItem'],
                    'default',
                    'value' =>
                        function (Report $model) {
                            $periodItem = $model->getRequest()->getPeriodItem();
                            if (!empty($periodItem)) {
                                return $periodItem;
                            }
                            $daysCount = (integer)floor(
                                (
                                    $model->getRequest()->getDateTo()
                                    - $model->getRequest()->getDateFrom()
                                ) / 86400
                            );

                            return $model::getPeriodItemCode($daysCount);
                        }
                ],
            ], $this->reportRules()
        );
    }

    /**
     * @return ReportRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param ReportRequest $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function getYAxis()
    {
        return $this->yAxis;
    }

    /**
     * @param array $yAxis
     */
    public function setYAxis($yAxis)
    {
        $this->yAxis = $yAxis;
    }

    /**
     * @return array
     */
    public function getXAxis()
    {
        return $this->xAxis;
    }

    /**
     * @param array $xAxis
     */
    public function setXAxis($xAxis)
    {
        $this->xAxis = $xAxis;
    }


    /**
     * @param string $variable
     * @param array  $data
     *
     * @return $this
     * @throws ReportException
     */
    public function extendArray($variable, $data)
    {
        if (!is_array($this->$variable)) {
            throw new ReportException("Property '$variable' is not a array");
        }
        $this->$variable = array_replace_recursive($this->$variable, $data);

        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getPeriodItem()
    {
        return $this->periodItem;
    }

    /**
     * @param string $periodItem
     */
    public function setPeriodItem($periodItem)
    {
        $this->periodItem = $periodItem;
    }


    public static function getReportParams()
    {
        $libraryFilter = function (ActiveQuery $query) {
            /** @todo ������� ���������� ��������� ���������� ������� */
            $libraryId = \Yii::$app->request->get('library_id');
            $query->andWhere(['library_id' => (integer)$libraryId]);
        };

        return [
            'neb_user_registers'                            => [
                'action'    => 'neb_user_registers',
                'report'    => 'CountActionsTable',
                'aggregate' => 'AggregatedCount',
                'meta'      => [
                    'totalCount' => true,
                ]
            ],
            'neb_user_active'                               => [
                'report'      => 'CountActionsTable',
                'action'      => 'neb_user_auth',
                'aggregate'   => 'AggregatedCount',
                'meta'        => [
                    'totalCount' => true,
                ],
                'modifyQuery' => function (ActiveQuery $query) {
                    $select = $query->select;
                    if (!empty($select)) {
                        $key = array_search('COUNT(id) cnt', $select);
                        if (false !== $key)
                            $select[$key] = 'COUNT(DISTINCT user_id) cnt';
                        $query->select($select);
                    }
                }
            ],
            'neb_user_verifications'                        => [
                'action'    => 'neb_user_verifications',
                'report'    => 'CountActionsTable',
                'aggregate' => 'AggregatedCount',
                'meta'      => [
                    'totalCount' => true,
                ]
            ],
            'neb_book_page_view'                            => [
                'action'    => 'neb_book_page_view',
                'report'    => 'CountActionsTable',
                'aggregate' => 'AggregatedCount',
                'meta'      => [
                    'totalCount' => true,
                ]
            ],
            'neb_book_card_view'                            => [
                'action'    => 'neb_book_card_view',
                'report'    => 'CountActionsTable',
                'aggregate' => 'AggregatedCount',
                'meta'      => [
                    'totalCount' => true,
                ]
            ],
            'neb_book_card_view_closed'                     => [
                'report'      => 'CountActionsSubTable',
                'action'      => 'neb_book_card_view',
                'aggregate'   => 'AggregatedCount',
                'meta'        => [
                    'totalCount' => true,
                ],
                'modifyQuery' => function (ActiveQuery $query) {
                    $query->andWhere(['closed' => 1]);
                },
            ],
            'neb_book_document_view'                        => [
                'report'      => 'CountActionsTable',
                'action'      => 'neb_book_view',
                'aggregate'   => 'AggregatedCount',
            ],
            'neb_book_document_unique'                      => [
                'report'      => 'CountActionsSubTable',
                'action'      => 'neb_book_page_view',
                'aggregate'   => 'AggregatedCount',
                'meta'        => [
                    'totalCount' => true,
                ],
                'modifyQuery' => function (ActiveQuery $query) {
                    $select = $query->select;
                    if (!empty($select)) {
                        $key = array_search('COUNT(action.id) cnt', $select);
                        if (false !== $key)
                            $select[$key] = 'COUNT(DISTINCT book_id) cnt';
                        $query->select($select);
                    }
                    $query->andWhere(['page_num' => 1]);
                },
            ],
            'neb_book_downloads'                            => [
                'action'    => 'neb_book_downloads',
                'report'    => 'CountActionsTable',
                'aggregate' => 'AggregatedCount',
                'meta'      => [
                    'totalCount' => true,
                ],
            ],
            'neb_feedback_count'                            => [
                'action'    => 'neb_feedback_count',
                'report'    => 'CountActionsTable',
                'aggregate' => 'AggregatedCount',
                'meta'      => [
                    'totalCount' => true,
                ],
            ],
            'neb_search_queries'                            => [
                'action'    => 'neb_search_queries',
                'report'    => 'CountActionsTable',
                'aggregate' => 'AggregatedCount',
                'meta'      => [
                    'totalCount' => true,
                ],
            ],
            'neb_books_count'                               => [
                'type'       => 'library_books',
                'report'     => 'PeriodDataSubTable',
                'period'     => 'daily',
                'groupDates' => 'max',
            ],
            'neb_library_books_count'                       => [
                'type'        => 'library_books',
                'report'      => 'PeriodDataSubTable',
                'period'      => 'daily',
                'groupDates'  => 'max',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_library_books_digitized_count'             => [
                'type'        => 'library_books_digitized',
                'report'      => 'PeriodDataSubTable',
                'period'      => 'daily',
                'groupDates'  => 'max',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_book_library_document_view'                => [
                'report'      => 'CountActionsSubTable',
                'action'      => 'neb_book_page_view',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $query->andWhere(['page_num' => 1]);
                    $libraryFilter($query);
                },
            ],
            'neb_book_library_downloads'                    => [
                'report'      => 'CountActionsSubTable',
                'action'      => 'neb_book_downloads',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_library_service_cnt_book_downloads'        => [
                'type'        => 'library_service_cnt_book_downloads',
                'report'      => 'PeriodDataSubTable',
                'period'      => 'daily',
                'groupDates'  => 'max',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_library_service_cnt_book_views'            => [
                'type'        => 'library_service_cnt_book_views',
                'report'      => 'PeriodDataSubTable',
                'period'      => 'daily',
                'groupDates'  => 'max',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_library_service_cnt_book_downloads_unique' => [
                'type'        => 'library_service_cnt_book_downloads_unique',
                'report'      => 'PeriodDataSubTable',
                'period'      => 'daily',
                'groupDates'  => 'max',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_library_service_cnt_book_views_unique'     => [
                'type'        => 'library_service_cnt_book_views_unique',
                'report'      => 'PeriodDataSubTable',
                'period'      => 'daily',
                'groupDates'  => 'max',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_user_active_library'                       => [
                'report'      => 'CountActionsSubTable',
                'action'      => 'neb_user_auth',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $select = $query->select;
                    if (!empty($select)) {
                        $key = array_search('COUNT(action.id) cnt', $select);
                        if (false !== $key)
                            $select[$key] = 'COUNT(DISTINCT user_id) cnt';
                        $query->select($select);
                    }
                    $libraryFilter($query);
                }
            ],
            'neb_user_deletions'                            => [
                'report'    => 'CountActionsTable',
                'action'    => 'neb_user_deletions',
                'aggregate' => 'AggregatedCount',
            ],
            'neb_library_user_deletions'                    => [
                'report'      => 'CountActionsSubTable',
                'action'      => 'neb_user_deletions',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_library_user_registers'                    => [
                'type'        => 'library_user_registers',
                'report'      => 'PeriodDataSubTable',
                'period'      => 'daily',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_book_library_page_view'                    => [
                'action'      => 'neb_book_page_view',
                'report'      => 'CountActionsSubTable',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_library_search_queries'                    => [
                'action'      => 'neb_search_queries',
                'report'      => 'CountActionsSubTable',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_library_user_bindings'                     => [
                'action'      => 'neb_user_bindings',
                'report'      => 'CountActionsSubTable',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $libraryFilter($query);
                },
            ],
            'neb_user_registers_all_wchz'                   => [
                'actionName' => 'neb_user_registers',
                'report'     => 'AllWchzReport',
            ],
            'neb_user_active_all_wchz'                      => [
                'actionName' => 'neb_user_auth',
                'sqlCnt'     => 'DISTINCT neb_action.user_id',
                'report'     => 'AllWchzReport',
                'modifyQuery' => function (ActiveQuery $query)
                use ($libraryFilter) {
                    $select = $query->select;
                    if (!empty($select)) {
                        $key = array_search('count(DISTINCT neb_action.id) cnt', $select);
                        if (false !== $key)
                            $select[$key] = 'COUNT(DISTINCT CONCAT(DATE_FORMAT(neb_action.timestamp, "%Y-%m-%d"), neb_action.user_id)) cnt';
                        $query->select($select);
                    }
                }
            ],
            'neb_book_page_view_all_wchz'                   => [
                'actionName' => 'neb_book_page_view',
                'report'     => 'AllWchzReport',
            ],
            'neb_book_document_view_all_wchz'               => [
                'actionName'  => 'neb_book_page_view',
                'report'      => 'AllWchzReport',
                'sqlCnt'      => 'neb_book_action.action_id',
                'modifyQuery' => function (ActiveQuery $query) {
                    $query->leftJoin(
                        'neb_book_action', <<<SQL
neb_book_action.action_id = neb_action.id
AND neb_book_action.page_num = 1
SQL
                    );
                },
            ],
            'neb_book_document_unique_all_wchz'             => [
                'actionName'  => 'neb_book_page_view',
                'report'      => 'AllWchzReport',
                'sqlCnt'      => 'DISTINCT neb_book_action.book_id',
                'modifyQuery' => function (ActiveQuery $query) {
                    $query->leftJoin(
                        'neb_book_action', <<<SQL
neb_book_action.action_id = neb_action.id
AND neb_book_action.page_num = 1
SQL
                    );
                },
            ],
            'neb_book_downloads_all_wchz'                   => [
                'actionName' => 'neb_book_downloads',
                'report'     => 'AllWchzReport',
            ],
            'neb_feedback_count_all_wchz'                   => [
                'actionName' => 'neb_feedback_count',
                'report'     => 'AllWchzReport',
            ],
            'neb_search_queries_all_wchz'                   => [
                'actionName' => 'neb_search_queries',
                'report'     => 'AllWchzReport',
            ],
            'neb_user_verifications_all_wchz'               => [
                'actionName' => 'neb_user_verifications',
                'report'     => 'AllWchzReport',
            ],
            'neb_book_card_view_all_wchz'                   => [
                'actionName' => 'neb_book_card_view',
                'report'     => 'AllWchzReport',
            ],
            'neb_book_card_view_closed_all_wchz'            => [
                'actionName'  => 'neb_book_card_view',
                'report'      => 'AllWchzReport',
                'sqlCnt'      => 'neb_book_action.action_id',
                'modifyQuery' => function (ActiveQuery $query) {
                    $query->leftJoin(
                        'neb_book_action', <<<SQL
neb_book_action.action_id = neb_action.id
AND neb_book_action.closed = 1
SQL
                    );
                },
            ],
            'neb_search_frequency_report'                   => [
                'action' => 'neb_search_queries',
                'report' => 'SearchFrequencyReport',
            ],
            'neb_search_queries_timestamp_interval'         => [
                'action' => 'neb_search_queries',
                'report' => 'ActionDatesInterval',
            ],
            'neb_search_queries_frequency_all'              => [
                'action' => 'neb_search_queries',
                'report' => 'SearchFrequencyAllReport',
            ],
        ];
    }

    /**
     * @param ReportRequest $request
     *
     * @return static
     * @throws ReportException
     */
    public static function factory(ReportRequest $request)
    {
        $reportParams = static::getReportParams();
        $reportCode = $request->getReport();
        if (!isset($reportParams[$reportCode])) {
            throw new ReportException("Report '$reportCode' not found");
        }
        $aggregated = false;
        if (isset($reportParams[$reportCode]['aggregate'])
            && !$request->getIp() && !$request->getWchzId()
        ) {
            $class = $reportParams[$reportCode]['aggregate'];
            $aggregated = true;
        } else {
            $class = $reportParams[$reportCode]['report'];
        }
        $class = 'app\models\Stats\Report\\' . $class;

        $params = $reportParams[$reportCode];
        $params['request'] = $request;
        if (empty($params['action'])) {
            $params['action'] = $reportCode;
        }
        if ($aggregated) {
            $params['action'] = $reportCode;
        }
        unset($params['report']);
        unset($params['aggregate']);

        /** @var Report $report */
        $report = new $class($params);
        $report->load($params, '');
        $report->validate();

        return $report;
    }
}