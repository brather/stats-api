<?php
/**
 * User: agolodkov
 * Date: 19.01.2016
 * Time: 17:29
 */

namespace app\models\Stats;


use yii\base\Model;

class ReportRequest extends Model
{
    /**
     * @var int
     */
    public $dateFrom;

    /**
     * @var int
     */
    public $dateTo;

    /**
     * @var []
     */
    public $ip = [];

    /**
     * @var int
     */
    public $wchzId;

    /**
     * @var string
     */
    public $dayFormat = 'd.m.Y';

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $report;

    /**
     * @var string
     */
    public $periodItem;

    /**
     * @var int
     */
    public $limit;

    /**
     * @var int
     */
    public $offset;


    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'filter', 'filter' => 'strtotime'],
            [
                [
                    'ip',
                    'name',
                    'dayFormat',
                    'periodItem',
                ],
                'safe'
            ],
            [['ip'], 'default', 'value' => []],
            [['dateTo'], 'default', 'value' => time()],
            [['dateFrom'], 'default', 'value' =>
                function (ReportRequest $model) {
                    return $model->getDateTo() - 86400 * 30;
                }],
            [
                [
                    'dateFrom',
                    'dateTo',
                    'limit',
                    'offset',
                    'wchzId'
                ],
                'integer'
            ],
            [['report'], 'required'],
        ];
    }

    /**
     * @return int
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @param int $dateFrom
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return int
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @param int $dateTo
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return string
     */
    public function getDayFormat()
    {
        return $this->dayFormat;
    }

    /**
     * @param string $dayFormat
     */
    public function setDayFormat($dayFormat)
    {
        $this->dayFormat = $dayFormat;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getReport()
    {
        return $this->report;
    }


    public function getAction()
    {
        return $this->getReport();
    }

    /**
     * @param string $report
     */
    public function setReport($report)
    {
        $this->report = $report;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getPeriodItem()
    {
        return $this->periodItem;
    }

    /**
     * @param string $periodItem
     */
    public function setPeriodItem($periodItem)
    {
        $this->periodItem = $periodItem;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getWchzId()
    {
        return $this->wchzId;
    }

    /**
     * @param int $wchzId
     */
    public function setWchzId($wchzId)
    {
        $this->wchzId = $wchzId;
    }
}