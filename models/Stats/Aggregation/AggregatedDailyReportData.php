<?php
/**
 * User: agolodkov
 * Date: 29.06.2016
 * Time: 12:37
 */

namespace app\models\Stats\Aggregation;


use yii\db\ActiveRecord;

/**
 * Class AggregatedDailyReportData
 *
 * @package app\models\Stats\Aggregation
 * @property $report_code string
 * @property $date string
 * @property $value string
 */
class AggregatedDailyReportData extends ActiveRecord
{
    /**
     * @var int
     */
    public $cnt;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_aggregated_daily_report_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_code', 'date', 'value'], 'required'],
            [['report_code'], 'string'],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
            [['value'], 'integer'],
            [['value'], 'filter', 'filter' => 'intval'],
        ];
    }
}