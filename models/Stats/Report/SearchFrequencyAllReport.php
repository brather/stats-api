<?php
/**
 * User: agolodkov
 * Date: 04.10.2016
 * Time: 16:55
 */

namespace app\models\Stats\Report;


use app\models\Action\DataValues;
use app\models\Stats\Report;
use app\models\Stats\ReportInterface;
use yii\db\ActiveQuery;

class SearchFrequencyAllReport extends Report implements ReportInterface
{

    public $result;

    public function loadData()
    {
        $query = $this->_buildQuery();
        $this->_prepareQuery($query);
        $result = $query->all();
        /** @var DataValues $item */
        foreach ($result as $key => $item) {
            $result[$key] = $item->toArray();
        }

        $this->result = $result;
    }

    protected function _buildQuery()
    {
        $query = $this->_forgeQuery();

        $query->select(
            [
                'data',
                'value',
            ]
        );
        $query->where(
            ['code' => $this->getAction()]
        )->orderBy(['value' => SORT_DESC]);

        $request = $this->getRequest();
        $limit = $request->getLimit();
        $offset = $request->getOffset();
        if ($limit > 0) {
            $query->limit($limit);
        }
        if ($offset > 0) {
            $query->offset($offset);
        }

        return $query;
    }

    protected function _prepareQuery(ActiveQuery $query)
    {
    }

    /**
     * @return ActiveQuery
     */
    protected function _forgeQuery()
    {
        return DataValues::find();
    }
}