<?php
/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 17:02
 */

namespace app\models\Stats\Report;


use app\models\PeriodData\PeriodData;
use yii\db\ActiveQuery;

/**
 * Class PeriodDataTable
 *
 * @todo    отделить от CountActionsTable, сделать доп. абстракцию
 *
 * @package app\models\Stats\Report
 */
class PeriodDataTable extends CountActionsTable
{

    /**
     * @var string
     */
    protected $period;

    /**
     * @var string
     */
    protected $type;

    /**
     * @return ActiveQuery
     */
    protected function _forgeQuery()
    {
        return PeriodData::factoryTableDataModel(
            $this->getPeriod()
        )->find();
    }

    /**
     * @param ActiveQuery $query
     *
     * @return array
     */
    protected function _getResult(ActiveQuery $query)
    {
        $result = $query->all();
        /** @var PeriodData $item */
        foreach ($result as $key => $item) {
            $max = $item->max;
            $date = $item->date;
            $item = $item->toArray();
            $item['value'] = $max;
            $item['timestamp'] = $date;
            $result[$key] = $item;
        }

        return $result;
    }

    protected function _buildQuery()
    {
        $query = $this->_forgeQuery();

        $query->select(
            [
                $this->getSqlPart('selectMax'),
                $this->getSqlPart('selectDate'),
            ]
        );

        $query->where(
            ['type' => $this->getType()]
        )->andWhere(
            [
                '>=',
                $this->getSqlPart('date'),
                date(
                    'Y-m-d',
                    $this->getRequest()->getDateFrom()
                ),
            ]
        )->andWhere(
            [
                '<=',
                $this->getSqlPart('date'),
                date(
                    'Y-m-d',
                    $this->getRequest()->getDateTo()
                ),
            ]
        )->groupBy($this->getSqlPart('date'))
            ->orderBy($this->getSqlPart('date'));

        $this->_applyIpAddressFilter($query, $this->getSqlPart('ip'));

        return $query;
    }

    protected function _initSqlParts()
    {
        $this->_sqlParts = array_replace(
            $this->_sqlParts,
            [
                'selectMax'  => 'MAX(' . $this->getSqlPart('value_num')
                    . ') max',
                'selectDate' => '' . $this->getSqlPart('date') . ' date',
            ]
        );
        parent::_initSqlParts();
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param string $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


}