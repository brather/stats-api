<?php
/**
 * User: agolodkov
 * Date: 08.02.2016
 * Time: 18:11
 */

namespace app\models\Stats\Report;


use app\models\Action\Action;
use app\models\Stats\Report;
use app\models\WchzList;
use yii\db\ActiveQuery;

class AllWchzReport extends Report
{

    public $sqlCnt = 'neb_action.action';
    public $actionName;

    /**
     * @var callable
     */
    public $modifyQuery;


    public function loadData()
    {
        $query = $this->_buildQuery();

        $result = $query->all();
        $labels = [];
        $values = [];
        foreach ($result as $item) {
            $labels[] = $item['wchzName'];
            $values[] = $item['cnt'];
        }
        $this->extendArray('yAxis', ['labels' => $labels]);
        $this->extendArray('xAxis', ['series' => [['data' => $values]]]);

        return $this;
    }

    protected function _prepareQuery(ActiveQuery $query)
    {
        if (is_callable($this->modifyQuery)) {
            call_user_func_array($this->modifyQuery, [$query]);
        }
    }

    protected function _buildQuery()
    {
        $action = $this->actionName;
        $sqlCnt = $this->sqlCnt;
        $wchzTable = WchzList::tableName();
        $timestampFrom = date(
            'Y-m-d 00:00:00',
            $this->getRequest()->getDateFrom()
        );
        $timestampTo = date(
            'Y-m-d 23:59:59',
            $this->getRequest()->getDateTo()
        );
        $query = $this->_forgeQuery();

        $query->select(
            [
                "count(DISTINCT neb_action.id) cnt",
                "$wchzTable.name               wchzName"
            ]
        )
            ->indexBy('wchzName')
            ->leftJoin(
                $wchzTable, "neb_action.wchz_id = $wchzTable.wchz_id"
            )
            ->where(['action' => $action])
            ->andWhere(
                [
                    '>=',
                    'timestamp',
                    $timestampFrom,
                ]
            )
            ->andWhere(
                [
                    '<=',
                    'timestamp',
                    $timestampTo,
                ]
            )
            ->groupBy($wchzTable . '.wchz_id');

        if (is_callable($this->modifyQuery)) {
            call_user_func_array($this->modifyQuery, [$query]);
        }

        return $query;
    }


    /**
     * @return ActiveQuery
     */
    protected function _forgeQuery()
    {
        return Action::find();
    }
}