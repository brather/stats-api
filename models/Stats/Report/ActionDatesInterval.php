<?php
/**
 * User: agolodkov
 * Date: 04.10.2016
 * Time: 18:41
 */

namespace app\models\Stats\Report;


use app\models\Action\Action;
use app\models\Stats\Report;
use app\models\Stats\ReportInterface;
use yii\db\ActiveQuery;

class ActionDatesInterval extends Report implements ReportInterface
{

    public $maxTimestamp;
    public $minTimestamp;

    public function loadData()
    {
        $query = $this->_forgeQuery();
        $query
            ->where(['action' => $this->getAction()])
            ->orderBy(['timestamp' => SORT_ASC])
            ->limit(1);
        $action = $query->one();
        $this->minTimestamp = $action->timestamp;

        $query = $this->_forgeQuery();
        $query
            ->where(['action' => $this->getAction()])
            ->orderBy(['timestamp' => SORT_DESC])
            ->limit(1);
        $action = $query->one();
        $this->maxTimestamp = $action->timestamp;
    }

    /**
     * @return ActiveQuery
     */
    protected function _forgeQuery()
    {
        return Action::find();
    }

    protected function _buildQuery()
    {
    }

    protected function _prepareQuery(ActiveQuery $query)
    {
    }
}