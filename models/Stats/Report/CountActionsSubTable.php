<?php
/**
 * User: agolodkov
 * Date: 21.01.2016
 * Time: 12:58
 */

namespace app\models\Stats\Report;


use app\models\Action\Action;
use yii\db\ActiveQuery;

class CountActionsSubTable extends CountActionsTable
{

    protected $_sqlParts
        = [
            'timestamp' => 'action.timestamp',
            'id'        => 'action.id',
            'ip'        => 'action.ip',
        ];

    /**
     * @return ActiveQuery
     */
    protected function _forgeQuery()
    {
        return Action::factoryActionModel($this->getAction())
            ->find()
            ->joinWith('action');
    }
}