<?php
/**
 * User: agolodkov
 * Date: 04.10.2016
 * Time: 16:55
 */

namespace app\models\Stats\Report;


use app\models\Action\SearchFrequencyAction;
use app\models\Stats\Report;
use app\models\Stats\ReportInterface;
use yii\db\ActiveQuery;

class SearchFrequencyReport extends Report implements ReportInterface
{

    public $result;

    public function loadData()
    {
        $query = $this->_buildQuery();
        $this->_prepareQuery($query);
        $result = $query->all();
        /** @var SearchFrequencyAction $item */
        foreach ($result as $key => $item) {
            $cnt = $item->getCnt();
            $timestamp = $item->timestamp;
            /** @todo ����������� ������ �� �������� ��� ���� */
            $item = $item->toArray();
            $item['value'] = $cnt;
            $item['timestamp'] = $timestamp;
            $result[$key] = $item;
        }

        $this->result = $result;
    }

    protected function _buildQuery()
    {
        $query = $this->_forgeQuery();

        $query->select(
            [
                'COUNT(*) cnt',
                'query',
            ]
        );
        $query->where(
            ['action' => 'neb_search_queries']
        )->andWhere(
            [
                '>=',
                'action.timestamp',
                date(
                    'Y-m-d 00:00:00',
                    $this->getRequest()->getDateFrom()
                ),
            ]
        )->andWhere(
            [
                '<=',
                'action.timestamp',
                date(
                    'Y-m-d 23:59:59',
                    $this->getRequest()->getDateTo()
                ),
            ]
        )->groupBy('query')
            ->orderBy(['cnt' => SORT_DESC]);

        return $query;
    }

    protected function _prepareQuery(ActiveQuery $query)
    {
    }

    /**
     * @return ActiveQuery
     */
    protected function _forgeQuery()
    {
        return SearchFrequencyAction::find()
            ->joinWith('action');
    }
}