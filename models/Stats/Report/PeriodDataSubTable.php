<?php
/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 17:03
 */

namespace app\models\Stats\Report;


use app\models\PeriodData\PeriodData;
use yii\db\ActiveQuery;

class PeriodDataSubTable extends PeriodDataTable
{
    protected $_sqlParts
        = [
            'date' => 'period_data.date',
            'id'   => 'period_data.id',
        ];

    /**
     * @return ActiveQuery
     */
    protected function _forgeQuery()
    {
        return PeriodData::factoryDataModel($this->getType())
            ->find()
            ->joinWith($this->getPeriod());
    }
}