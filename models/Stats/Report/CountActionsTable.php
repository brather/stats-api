<?php
/**
 * User: agolodkov
 * Date: 20.01.2016
 * Time: 15:39
 */

namespace app\models\Stats\Report;


use app\models\Action\Action;
use app\models\Stats\Report;
use yii\db\ActiveQuery;

class CountActionsTable extends Report
{

    /**
     * @var callable
     */
    public $modifyQuery;

    /**
     * @var string
     */
    public $groupDates = 'sum';

    /**
     * @var array
     */
    protected $_sqlParts = [];
    protected $_dbResult = [];

    public function reportRules()
    {
        return [
            [['modifyQuery'], 'safe']
        ];
    }

    public function init()
    {
        $this->_initSqlParts();
    }

    public function loadData()
    {
        $query = $this->_buildQuery();
        $this->_prepareQuery($query);
        $this->_dbResult = $this->_getResult($query);

        $labels = $this->_buildLabels();
        $values = array_fill(0, count($labels), 0);
        $labelKeys = array_flip(array_values($labels));
        $series = [0 => ['data' => $values]];

        $dateFormat = $this->getRequest()->getDayFormat();
        if ('month' === $this->getPeriodItem()) {
            $dateFormat = 'LLLL YYYY';
        }

        /** @todo сопоставление дат с данными выгялдит страшно, вытащить куда-нибудь в класс */
        /** @todo сделать несколько линий, сейчас одна ($series[0]) */
        /** @var Action $item */
        foreach ($this->_dbResult as $key => $item) {
            $dateKey = date($dateFormat, strtotime($item['timestamp'])); //\Yii::$app->getFormatter()->asDate($item['timestamp'], $dateFormat);

            if (!isset($labelKeys[$dateKey])) {
                /** Кастыль для дат приходящих из mysql */
                $timestamp = strtotime($item['timestamp']);
                $lastLabKey = 0;
                foreach ($labels as $labelTimestamp => $label) {
                    if ($labelTimestamp >= $timestamp
                        && $lastLabKey < $timestamp
                    ) {
                        $dateKey = $label;
                        break;
                    }
                    $lastLabKey = $labelTimestamp;
                }
            }

            if (isset($labelKeys[$dateKey])) {
                switch ($this->groupDates) {
                    case 'max':
                        if (
                            !isset($series[0]['data'][$labelKeys[$dateKey]])
                            || $series[0]['data'][$labelKeys[$dateKey]]
                            < (integer)$item['value']
                        ) {
                            $series[0]['data'][$labelKeys[$dateKey]]
                                = (integer)$item['value'];
                        }
                        break;
                    case 'sum':
                    default:
                        if (isset($series[0]['data'][$labelKeys[$dateKey]])) {
                            $series[0]['data'][$labelKeys[$dateKey]]
                                += (integer)$item['value'];
                        } else {
                            $series[0]['data'][$labelKeys[$dateKey]]
                                = (integer)$item['value'];
                        }
                        break;
                }
            }
        }
        $series[0]['data'] = array_values($series[0]['data']);
        $labels = array_values($labels);

        $this->extendArray('yAxis', ['labels' => $labels]);
        $this->extendArray('xAxis', ['series' => $series]);

        $this->_loadMetaData();

        return $this;
    }

    /**
     * @param ActiveQuery $query
     *
     * @return array
     */
    protected function _getResult(ActiveQuery $query)
    {
        $result = $query->all();
        /** @var Action $item */
        foreach ($result as $key => $item) {
            $cnt = $item->getCnt();
            $timestamp = $item->timestamp;
            /** @todo разобраться почему не отдаются все поля */
            $item = $item->toArray();
            $item['value'] = $cnt;
            $item['timestamp'] = $timestamp;
            $result[$key] = $item;
        }

        return $result;
    }

    /**
     * @todo делал немного в спешке, сделать красивее и понятнее
     * @return array
     */
    protected function _buildLabels()
    {
        $dateFrom = new \DateTime();
        $dateFrom->setTimestamp($this->getRequest()->getDateFrom());
        $dateTo = new \DateTime();
        $dateTo->setTimestamp($this->getRequest()->getDateTo());

        $labels = [];
        $interVal = 'P1D';
        $modify = null;
        $dayMod = 0;
        $format = $this->getRequest()->getDayFormat();
        /** @todo  для недель сделать что то с последним днем года - не считается */
        switch ($this->getPeriodItem()) {
            case 'twoDays':
                $interVal = 'P1D';
                $dayMod = 2;
                break;
            case 'fourDays':
                $interVal = 'P1D';
                $dayMod = 4;
                break;
            case 'week':
                $interVal = 'P1W';
                $modify = 'Sunday this week';
                break;
            case 'twoWeeks':
                $interVal = 'P2W';
                $tmpDateFrom = clone $dateFrom;
                $tmpDateFrom->modify('+1 day');
                /** Откуда должена начинаться точка */
                if (0 === $tmpDateFrom->format('W') % 2) {
                    $modify = 'Sunday last week';
                } else {
                    $modify = 'Sunday this week';
                }
                break;
            case 'month':
                $interVal = 'P1M';
                $modify = 'last day of this month';
                $format = 'LLLL YYYY';
                break;
        }

        /** @todo что то пошло не так с таймзонами, разобраться */
        $dateTo->modify('+4 hour');
        $period = new \DatePeriod(
            $dateFrom, new \DateInterval($interVal), $dateTo
        );

        /** @var \DateTime $date */
        foreach ($period as $date) {
            if (
                $dayMod > 0
                && 0 !== ((integer)$date->format('z') + 1) % $dayMod
                && $date->format('z') !== date(
                    'z',
                    strtotime('last day of december', $date->getTimestamp())
                )
            ) {
                continue;
            }

            if (null !== $modify) {
                $date->modify($modify);
            }

            $labels[$date->getTimestamp()] = date($format, $date->getTimestamp()); //\Yii::$app->getFormatter()->asDate($date->getTimestamp(), $format);

        }

        return $labels;
    }

    protected function _prepareQuery(ActiveQuery $query)
    {
        if (is_callable($this->modifyQuery)) {
            call_user_func_array($this->modifyQuery, [$query]);
        }
    }

    protected function _buildQuery()
    {
        $query = $this->_forgeQuery();

        $query->select(
            [
                $this->getSqlPart('select' . ucfirst($this->getPeriodItem())),
                $this->getSqlPart('selectCnt'),
            ]
        );
        if ('month' === $this->getPeriodItem()) {
            $query->addSelect($this->getSqlPart('selectLastDayTimestamp'));
        } else {
            $query->addSelect($this->getSqlPart('selectMaxTimestamp'));
        }

        $query->where(
            ['action' => $this->getAction()]
        );
        if ($wchzId = $this->getRequest()->getWchzId()) {
            $query->andWhere(
                [
                    '=',
                    'wchz_id',
                    $wchzId,
                ]
            );
        }
        $query->andWhere(
            [
                '>=',
                $this->getSqlPart('timestamp'),
                date(
                    'Y-m-d 00:00:00',
                    $this->getRequest()->getDateFrom()
                ),
            ]
        )->andWhere(
            [
                '<=',
                $this->getSqlPart('timestamp'),
                date(
                    'Y-m-d 23:59:59',
                    $this->getRequest()->getDateTo()
                ),
            ]
        )->groupBy($this->getPeriodItem())
            ->orderBy($this->getSqlPart('timestamp'));

        $this->_applyIpAddressFilter($query, $this->getSqlPart('ip'));

        return $query;
    }

    /**
     * @return ActiveQuery
     */
    protected function _forgeQuery()
    {
        return Action::find();
    }

    /**
     * @param string $code
     *
     * @return string
     */
    public function getSqlPart($code)
    {
        return isset($this->_sqlParts[$code]) ? $this->_sqlParts[$code] : $code;
    }

    /**
     * @todo выпилить это отсюда в ActiveQuery
     */
    protected function _initSqlParts()
    {
        $this->_sqlParts = array_replace(
            $this->_sqlParts,
            [
                'selectMaxDate'          => 'DATE_FORMAT(MAX('
                    . $this->getSqlPart('timestamp') . '), "%Y-%m-%d") day',

                'selectCnt'              => 'COUNT(' . $this->getSqlPart('id')
                    . ') cnt',

                'selectMaxTimestamp'     => 'MAX(' . $this->getSqlPart(
                        'timestamp'
                    )
                    . ') timestamp',

                'selectLastDayTimestamp' => 'LAST_DAY(' . $this->getSqlPart(
                        'timestamp'
                    )
                    . ') timestamp',

                'selectDay'              => 'DATE_FORMAT('
                    . $this->getSqlPart('timestamp') . ', "%Y-%m-%d") day',

                'selectTwoDays'          =>
                    'CONCAT(YEAR(' . $this->getSqlPart('timestamp') . '), "-", '
                    . '(DAYOFYEAR(' . $this->getSqlPart('timestamp')
                    . ') - 1) DIV 2'
                    . ') twoDays',

                'selectFourDays'         =>
                    'CONCAT(YEAR(' . $this->getSqlPart('timestamp') . '), "-", '
                    . '(DAYOFYEAR(' . $this->getSqlPart('timestamp')
                    . ') - 1) DIV 4'
                    . ') fourDays',

                'selectWeek'             => 'DATE_FORMAT('
                    . $this->getSqlPart('timestamp') . ', "%Y-%u") week',

                'selectTwoWeeks'         =>
                    'CONCAT(YEAR(' . $this->getSqlPart('timestamp') . '), "-", '
                    . 'WEEK(' . $this->getSqlPart('timestamp') . ', 1) DIV 2'
                    . ') twoWeeks',

                'selectMonth'            => 'DATE_FORMAT('
                    . $this->getSqlPart('timestamp') . ', "%Y-%m") month',
            ]
        );
    }


    /**
     * @param ActiveQuery $query
     * @param string      $field
     */
    protected function _applyIpAddressFilter(ActiveQuery $query, $field)
    {
        $filterWhere = [];
        foreach ($this->getRequest()->getIp() as $ip) {
            if (false === strpos($ip, '-')) {
                $filterWhere[] = ['=', $field, ip2long($ip)];
            } else {
                $ip = explode('-', $ip);
                $filterWhere[] = [
                    'and',
                    ['>=', $field, ip2long($ip[0])],
                    ['<=', $field, ip2long($ip[1])],
                ];
            }
        }
        array_unshift($filterWhere, 'or');
        $query->andFilterWhere($filterWhere);
    }
}