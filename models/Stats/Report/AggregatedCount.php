<?php
/**
 * User: agolodkov
 * Date: 29.06.2016
 * Time: 15:43
 */

namespace app\models\Stats\Report;


use app\models\Stats\Aggregation\AggregatedDailyReportData;
use yii\db\ActiveQuery;

class AggregatedCount extends CountActionsTable
{

    /**
     * @return ActiveQuery
     */
    protected function _forgeQuery()
    {
        return AggregatedDailyReportData::find();
    }

    protected function _buildQuery()
    {
        $query = $this->_forgeQuery();

        $query->select(
            [
                $this->getSqlPart('select' . ucfirst($this->getPeriodItem())),
                $this->getSqlPart('selectCnt'),
            ]
        );
        if ('month' === $this->getPeriodItem()) {
            $query->addSelect($this->getSqlPart('selectLastDayTimestamp'));
        } else {
            $query->addSelect($this->getSqlPart('selectMaxTimestamp'));
        }

        $query->where(
            ['report_code' => $this->getAction()]
        )->andWhere(
            [
                '>=',
                $this->getSqlPart('date'),
                date(
                    'Y-m-d',
                    $this->getRequest()->getDateFrom()
                ),
            ]
        )->andWhere(
            [
                '<=',
                $this->getSqlPart('date'),
                date(
                    'Y-m-d',
                    $this->getRequest()->getDateTo()
                ),
            ]
        )->groupBy($this->getPeriodItem())
            ->orderBy($this->getSqlPart('date'));

        $this->_applyIpAddressFilter($query, $this->getSqlPart('ip'));

        return $query;
    }

    protected function _prepareQuery(ActiveQuery $query)
    {
    }

    /**
     * @param ActiveQuery $query
     *
     * @return array
     */
    protected function _getResult(ActiveQuery $query)
    {
        $result = $query->all();
        /** @var AggregatedDailyReportData $item */
        foreach ($result as $key => $item) {
            $date = $item->date;
            $value = $item->cnt;
            $item = $item->toArray();
            $item['timestamp'] = $date;
            $item['value'] = $value;
            $result[$key] = $item;
        }

        return $result;
    }

    /**
     * @todo �������� ��� ������ � ActiveQuery
     */
    protected function _initSqlParts()
    {
        $this->_sqlParts['timestamp'] = 'date';
        parent::_initSqlParts();
        $this->_sqlParts = array_replace(
            $this->_sqlParts,
            [
                'selectMaxDate'          => 'DATE_FORMAT(MAX('
                    . $this->getSqlPart('timestamp') . '), "%Y-%m-%d") day',

                'selectCnt'              => 'SUM(' . $this->getSqlPart('value')
                    . ') cnt',

                'selectMaxTimestamp'     => 'MAX('
                    . $this->getSqlPart('timestamp') . ') date',

                'selectLastDayTimestamp' => 'LAST_DAY(' . $this->getSqlPart(
                        'timestamp'
                    )
                    . ') date',
            ]
        );
    }
}