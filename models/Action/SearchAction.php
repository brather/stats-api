<?php
/**
 * User: agolodkov
 * Date: 20.01.2016
 * Time: 16:50
 */

namespace app\models\Action;


class SearchAction extends ActionSubTable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_search_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id'], 'required'],
            [['action_id'], 'integer'],
            [['query'], 'required'],
            [['query'], 'string'],
            [['session_id'], 'string'],
            [['library_id'], 'integer'],
        ];
    }
}