<?php
/**
 * User: agolodkov
 * Date: 20.01.2016
 * Time: 16:48
 */

namespace app\models\Action;

use app\helpers\TableModel;
use app\models\WchzList,
    app\models\LibrariesList;

/**
 * Class Action
 *
 * @package app\models\Action
 *
 * @property int $timestamp
 * @property int $ip
 * @property int $wchz_id
 */
class Action extends ActionActiveRecord
{

    /**
     * @var ActionActiveRecord
     */
    public $actionModel;

    /**
     * @var int
     */
    public $cnt;

    /**
     * @var
     */
    public $wchzName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action'], 'required'],
            [['action'], 'string'],
            [['user_id', 'wchz_id'], 'filter', 'filter' => 'intval'],
            [['user_id', 'wchz_id'], 'integer'],
            [['ip'], 'filter', 'filter' => 'ip2long'],
            [['timestamp'], 'safe']
        ];
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public static function getActionTable($action)
    {
        $actionTables = [
            'neb_book_card_view'            => 'book_action',
            'neb_book_card_view_closed'     => 'book_action',
            'neb_book_document_view'        => 'book_action',
            'neb_book_document_unique'      => 'book_action',
            'neb_book_page_view'            => 'book_action',
            'neb_book_downloads'            => 'book_action',
            'neb_search_queries'            => 'search_action',
            'neb_user_bindings'             => 'user_bind_action',
            'neb_user_deletions'            => 'user_deletion_action',
            'neb_user_auth'                 => 'user_auth',
            'neb_book_change_access_action' => 'book_change_access_action',
        ];

        if (isset($actionTables[$action])) {
            return $actionTables[$action];
        }

        return '';
    }

    public static function getActionModelName($action)
    {
        return TableModel::getTableModelName(
            static::getActionTable($action),
            'app\models\Action\\'
        );
    }

    /**
     * @param $action
     *
     * @return ActionSubTable
     * @throws \Exception
     */
    public static function factoryActionModel($action)
    {
        $model = static::getActionModelName($action);
        if (!class_exists($model)) {
            throw new \Exception("Action model form '$action' not exists");
        }

        return new $model();
    }

    /**
     * @param string $action
     *
     * @return bool
     */
    public static function actionTableModelExists($action)
    {
        if (class_exists(static::getActionModelName($action))) {
            return true;
        }

        return false;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->_applyWchz();
            $this->_setNebLibraryId();

            return true;
        }

        return false;
    }

    protected function _setNebLibraryId()
    {
        if (
            isset($this->library_id) && !empty($this->library_id)
            && (
                !isset($this->neb_library_id) || empty($this->neb_library_id)
            )
        )
        {
            $librariesList = LibrariesList::find()
                ->select("id")
                ->where(
                    array( "=", "neb_partner_id", (int)$this->library_id )
                )->one();

            if (!$librariesList->hasErrors())
            {
                $this->neb_library_id = $librariesList->id;
            }
        }
    }

    protected function _applyWchz()
    {
        if ($this->ip > 0 && $this->wchz_id < 1) {
            /** @var WchzList $wchz */
            if ($wchz = WchzList::find()
                ->where([
                    'and',
                    ['=', 'ip', $this->ip],
                    ['=', 'active', 'Y'],
                ])
                ->orFilterWhere(
                    [
                        'and',
                        ['<=', 'ip', $this->ip],
                        ['>=', 'ip_to', $this->ip],
                        ['=', 'active', 'Y'],
                    ]
                )
                ->one()
            ) {
                $this->wchz_id = $wchz->wchz_id;
            }
        }
    }

    /**
     * @return ActionActiveRecord
     */
    public function getActionModel()
    {
        return $this->actionModel;
    }

    /**
     * @param ActionActiveRecord $actionModel
     */
    public function setActionModel($actionModel)
    {
        $this->actionModel = $actionModel;
    }

    /**
     * @return int
     */
    public function getCnt()
    {
        return $this->cnt;
    }

    /**
     * @param int $cnt
     */
    public function setCnt($cnt)
    {
        $this->cnt = $cnt;
    }

}