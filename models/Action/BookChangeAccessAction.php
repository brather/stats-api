<?php
/**
 * User: agolodkov
 * Date: 20.01.2016
 * Time: 15:40
 */

namespace app\models\Action;

use Yii;

class BookChangeAccessAction extends ActionSubTable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_book_change_access_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id'], 'required'],
            [['action_id'], 'integer'],
            [['library_id'], 'required'],
            [['library_id'], 'integer'],
            [['book_id'], 'required'],
            [['book_id'], 'string'],
            [['comment'], 'string'],
            [['error'], 'string'],
            [['closed'], 'boolean'],
        ];
    }
}