<?php
/**
 * User: agolodkov
 * Date: 20.01.2016
 * Time: 16:50
 */

namespace app\models\Action;


class SearchFrequencyAction extends ActionSubTable
{

    public $cnt;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_search_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query'], 'required'],
            [['query'], 'string'],
        ];
    }
}