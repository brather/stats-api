<?php
/**
 * User: agolodkov
 * Date: 20.01.2016
 * Time: 16:50
 */

namespace app\models\Action;


class DataValues extends ActionActiveRecord
{

    public $cnt;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_data_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query', 'data', 'value'], 'required'],
            [['query', 'data'], 'string'],
            [['value'], 'integer'],
        ];
    }
}