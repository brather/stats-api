<?php
/**
 * User: agolodkov
 * Date: 27.01.2016
 * Time: 14:31
 */

namespace app\models\Action;


class UserAuth extends ActionSubTable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_user_auth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id'], 'required'],
            [['action_id'], 'integer'],
            [['library_id'], 'integer'],
        ];
    }
}