<?php
/**
 * User: agolodkov
 * Date: 21.01.2016
 * Time: 13:03
 */

namespace app\models\Action;


use app\models\LibrariesList;

abstract class ActionSubTable extends ActionActiveRecord
{
    /**
     * @var int
     */
    public $timestamp;

    /**
     * @var int
     */
    public $cnt;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id'])
            ->from(Action::tableName() . ' action');
    }

    /**
     * @return int
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param int $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return int
     */
    public function getCnt()
    {
        return $this->cnt;
    }

    /**
     * @param int $cnt
     */
    public function setCnt($cnt)
    {
        $this->cnt = $cnt;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->_setNebLibraryId();

            return true;
        }

        return false;
    }

    private function _setNebLibraryId ()
    {
        if (
            isset($this->library_id) && !empty($this->library_id)
            && (
                !isset($this->neb_library_id) || empty($this->neb_library_id)
            )
        )
        {
            $librariesList = LibrariesList::find()
                ->select( "id" )
                ->where(
                    array( "=", "neb_partner_id", (int)$this->library_id )
                )->one();

            if (!$librariesList->id)
                $this->neb_library_id = null;
            else
                $this->neb_library_id = $librariesList->id;
        }

        return true;
    }

}