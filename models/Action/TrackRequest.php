<?php
/**
 * User: agolodkov
 * Date: 20.01.2016
 * Time: 10:44
 */

namespace app\models\Action;


use yii\base\Model;

class TrackRequest extends Model
{
    /**
     * @var string
     */
    public $action;

    /**
     * @var array
     */
    public $data;


    public function rules()
    {
        return [
            [['action'], 'string'],
            [['action', 'data'], 'required'],
        ];
    }

    /**
     * @return ActionActiveRecord
     */
    public function forgeActionModel()
    {
        $model = Action::factoryActionModel($this->getAction());
        $model->load($this->getData(), '');

        return $model;
    }

    /**
     * @return bool
     */
    public function modelExists()
    {
        return false !== Action::getActionModelName($this->getAction());
    }


    /**
     * @param string $dataField
     * @param mixed  $value
     */
    public function setDataItem($dataField, $value)
    {
        if (!is_array($this->data)) {
            $this->data = [];
        }
        $this->data[$dataField] = $value;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}