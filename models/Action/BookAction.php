<?php
/**
 * User: agolodkov
 * Date: 20.01.2016
 * Time: 15:40
 */

namespace app\models\Action;

use app\models\LibrariesList;
use Yii;

class BookAction extends ActionSubTable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_book_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id'], 'required'],
            [['action_id'], 'integer'],
            [['book_id'], 'required'],
            [['book_id'], 'string'],
            [['page_num'], 'filter', 'filter' => 'intval'],
            [['session_id'], 'string'],
            [['closed'], 'boolean'],
            [['library_id'], 'integer'],
        ];
    }
}