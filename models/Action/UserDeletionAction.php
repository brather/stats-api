<?php
/**
 * User: agolodkov
 * Date: 28.01.2016
 * Time: 14:21
 */

namespace app\models\Action;


class UserDeletionAction extends ActionSubTable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_user_deletion_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id'], 'required'],
            [['action_id'], 'integer'],
            [['library_id'], 'integer'],
        ];
    }
}