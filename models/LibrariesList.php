<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 27.10.2016
 * Time: 14:09
 */

namespace app\models;

use app\models\Action\Action,
    yii\db\ActiveRecord;

class LibrariesList extends ActiveRecord
{
    public $cnt;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_libraries_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array(
            array(
                array( 'name' ),
                'required'
            ),
            array(
                array( 'neb_partner_id' ),
                'integer'
            ),
            array(
                array( 'name' ),
                   'string'
            ),
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(
            Action::className(),
            array(
                'id' => 'action_id'
            )
        )->from( Action::tableName() . ' action' );
    }
}