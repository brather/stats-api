<?php
namespace app\models;

use app\models\Action\Action;
use yii\db\ActiveRecord;

/**
 * User: agolodkov
 * Date: 08.02.2016
 * Time: 16:54
 *
 * @property int $wchz_id
 */
class WchzList extends ActiveRecord
{
    public $cnt;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'neb_wchz_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wchz_id', 'ip'], 'required'],
            [['wchz_id', 'ip', 'ip_to', 'cnt', 'neb_library_id'], 'integer'],
            [['wchz_id', 'ip', 'ip_to', 'cnt', 'neb_library_id'], 'filter', 'filter' => 'intval'],
            [['name'],   'string'],
            [['active'], 'string'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id'])
            ->from(Action::tableName() . ' action');
    }
}